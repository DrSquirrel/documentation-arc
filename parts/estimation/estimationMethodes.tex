% ---the pose estimation problem.
Pose estimation is a general problem consisting of finding the attitude and the position of a given rigid object in 3D space. For flight control, it is essential to be able to estimate precisely the current pose at any moment. The knowledge of the attitude allows to convert from the body frame to the world frame and the knowledge of the position is needed for trajectory control. A common way to achieve this estimation is to use computer vision techniques by either equipping the object with a camera system or using an external system (e.g. MoCap).

In our case, the drone is not equipped with a camera. We do have a MoCap but the communication between the MoCap and the drone may fail and we do not want to only rely on it. Fortunately PX4 can use other kinds of sensors to estimate its pose.
% ---the pose estimation problem.


% --- hardware considerations
\paragraph{Hardware.}
Apart from the MoCap, we use only the IMU of the Pixracer to estimate the poses of the drones. The IMU provides real-time measurements of a triaxial gyroscope, of a triaxial accelerometer and of a triaxial magnetometer. These devices are based on MEMS technology. 

From the \tbf{gyroscope} it is possible to indirectly measure the three components of the angular velocity during a rotation. However the measurements are affected by wideband noise and a time-varying drift that cannot be corrected by calibration. An open loop integration on those measurements is therefore not recommended. Also, because all gyroscopes have a finite range of sensitivity, they can be saturated during highly dynamic motions. They need to be tuned so that they can handle the highest rates expected during the flight.

The \tbf{accelerometer} observes the IMU's proper acceleration and provides both the magnitude and direction of the total acceleration (including the gravitational acceleration). Once again, the measurements are affected by wideband noise, bias errors, and often by cross-coupling between the axes of the sensor (as the internal axes are not perfectly orthogonal). The TWOSTEP algorithm \cite{twostep} is the most common method to calibrate out the bias, the scale factor and non-orthogonality errors.

The \tbf{magnetometer} measures the projection of the direction of the surrounding magnetic field onto the body axes. Its measurements are corrupted by wideband noise and cross-coupling between the axes of the sensor (not perfectly orthogonal). Again the TWOSTEP algorithm can be used to calibrate those errors out. However, the ambient magnetic field measured by the magnetometer is greatly influenced by the surrounding environment. Metal structures are known to modify significantly the local magnetic field.
For developing our flying platform we used a space available in the former reactor hall R1 \cite{R1}. Down there the magnetic field was so much irregular that we had to deactivate the magnetometer of our drones.

So, because the data provided by the IMU is affected by high noise levels and time-varying biases, even after careful calibration, a sensor-fusion algorithm must be used to process the data to obtain a smooth and bias-free estimation of the pose while maintaining a low computational cost for running on the onboard processor.

We used the default calibration tools provided by PX4 to calibrate the sensors. We used the user interface of the ground control station \prog{QGroundControl} \cite{qgroundcontrol} to calibrate them almost effortlessly.
% --- hardware considerations

%---parametrizations
% p38 of MultAttEst
% The different parametrizations possible.
% For the position the parametrization is strainght forward: we use the 3 coordinates in a fixed frame.
\paragraph{Parametrization.}
Pose estimation consists of both attitude estimation and position estimation. While position parametrization is straightforward (we use the 3 coordinates in a fixed frame), there are many different parametrizations possible for the attitude. To give the attitude of the drone one has to give the rotation of the drone from its initial pose (with axes aligned with the reference frame) to its current pose. The most natural way to do so is as a rotation matrix. Other parametrizations include axis-angle, quaternions and Euler angles. Each parametrization has its pros and cons. Rotation matrix is unique and global but has 9 parameters for 3 degrees of freedom, while Euler angles use only 3 parameters but are not unique and not global. Quaternion parametrization is a good compromise as it has only 4 parameters, is global and computationally fast. The PX4 flight controller mainly uses quaternions which are sometimes converted to rotation matrices as follows: let $q = q_{0} + q_{1}i + q_{2}j + q_{3}k$, then the corresponding rotation matrix is:
% TALK ABOUT THE ADDITIVE AN D MULTIPLICATIVE WAYS WITH QUATERNIONS
\begin{equation}
\label{Eq:quaternionToRotMatrix}
R = \begin{bmatrix}
	1-2q_{2}^2-2q_{3}^2     & 2q_{1}q_{2}-2q_{0}q_{3} & 2q_{1}q_{3}+2q_{0}q_{2}\\
	2q_{1}q_{2}+2q_{0}q_{3} & 1-2q_{1}^2-2q_{3}^2     & 2q_{2}q_{3}-2q_{0}q_{1}\\
	2q_{1}q_{3}-2q_{0}q_{2} & 2q_{2}q_{3}+2q_{0}q_{1} & 1-2q_{1}^2-2q_{2}^2
\end{bmatrix}
\end{equation}
% - Rotation Matrix or Direction cosine Matrix (DCM)  
% - Axis-Angle
% - Quaternions - talk about the additive and multiplicative ways
% - Euler Angles
% The PX4 uses the quaternions.
%---parametrizations


\bigskip
%--- def terms general
% p163 chapter 10
In order to analyse the estimation methods we need to define some vocabulary. An estimation method is said to be \tbf{static} if it only uses the information available at the current time. It is said to be \tbf{dynamic} if it takes into account the dynamics of the system to update the previously estimated state. Also, an estimation method is \tbf{deterministic} if it uses the exact amount of information to get one unique solution. Such estimations are usually fast. An estimation method is \tbf{stochastic} if it uses more data than is necessary and that is possibly inconsistent (noisy). From this, it constructs a probabilistic model of the statistical distribution.
%--- def terms general

%--- attitude examples
Let's look at different methods to estimate attitude.
One deterministic and static method would assume that the drone has no acceleration other than gravity (i.e. it has no or little motion or it follows a uniform rectilinear motion). It would use acceleration to get the vertical vector and the magnetic field to get the North vector (this gives 2 columns of the matrix $R^{-1}$). Then the last vector could be deduced from the 2 previous vectors. However the accelerometer is usually very noisy and this method alone would give poor results.
The only case for an estimation to be deterministic and dynamic at the same time would be to use some initial knowledge of $R$ and integrate the kinematic equation using rotational speeds. However this cannot be used in practice due to the drift of the gyroscope.
Stochastic methods such as Kalman Filter allow us to combine these two measure methods to get interesting results. Usually such methods will smooth and try to approximate a solution from several equations with incompatible data (in that regard, the Kalman Filter can be viewed as an optimization filter).
We can remark that the particle filter is made for multimodal situations and doesn't seem to be a good choice in this case.
%--- attitude examples


%--- complete pose estimation
% For Pose estimation: chapter11
To estimate the pose (the attitude and the position) there are 2 approaches. Both of these approaches are implemented in the PX4 and we will describe one estimator of each kind in the following sections.
\begin{itemize}
	\item first estimate the attitude and then the position (\tbf{Q estimator and Local Position Estimator})
	\item estimate attitude and position simultaneously (\tbf{EKF2 estimator})
\end{itemize}
