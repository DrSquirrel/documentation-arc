\subsubsection{The EKF2 module of the PX4}
For the EKF2 estimator, the PX4 firmware uses the Estimation and Control Library (ECL) \cite{ECL} which uses an Extended Kalman Filter (EKF) algorithm to process sensor measurements and provide an estimate of the following states:
\begin{itemize}
	\setlength{\itemsep}{0em}
	\item Quaternion defining the rotation from North, East, Down local earth frame to X, Y, Z body frame
	\item Velocity at the IMU - North, East, Down (m/s)
	\item Position at the IMU - North, East, Down (m)
	\item IMU delta angle bias estimates - X, Y, Z (rad) (gyroscope bias)
	\item IMU delta velocity bias estimates - X, Y, Z (m/s) (accelerometer bias)
	\item Earth magnetic field components - North, East, Down (gauss)
	\item Vehicle body frame magnetic field bias - X, Y, Z (gauss)
	\item Wind velocity - North, East (m/s) (used only for fixed wing)
\end{itemize}

This library is located at \prog{Firmware/src/lib/ecl}, however it is created only when the firmware is built. The estimator module \prog{ekf2} calls that library. This estimator is now the recommended estimator for the PX4 but it is still under development (e.g. the MoCap support was added in August 2018).

In this estimator, both the position and the attitude are part of the state and thus are estimated simultaneously. As a consequence the dynamics are not linear. Indeed, the rotation matrix (computed from the quaternion, which is part of the state) is used to compute the frame change and to modify other elements of the state by multiplication.

A wide range of sensors are currently supported by EKF2 and various combinations of sensor measurements can be used (check \prog{https://docs.px4.io/master/en/advanced\_\penalty0 config/\penalty0 tuning\penalty0 \_the\_ecl\_ekf.html} for additional information regarding the use and the tuning of that estimator). On initialization, it checks for a minimum viable combination of sensors in order to initialize the attitude. 

There are several advantages in using that estimator:
\begin{itemize}
	\item It is able to fuse data from sensors with different time delays and data rates to improve accuracy during dynamic manoeuvres (once tuned correctly). Indeed, inspired by the paper \cite{timehorizon}, it takes into account the different time horizons of the sensors. There are two kinds of time characteristics of sensors: first the frequency at which they publish their measurements, and second the delay between the moment the measurement is made and the moment it is made available to the estimator. EKF2 takes into account both aspects in a very systematic way (see Figure~\ref{horizons}). It uses two different time horizons: the current time horizon, and the delayed time horizon which is delayed in the past by the maximum delay (sum of the period and the transmission delay) a measurement may have to guarantee that all active sensors have a measurement available. When measurements are received by the estimator, they are queued in a FIFO structure. Then at the delayed time horizon, the measurement fusion is made. A prediction step is used to propagate the states to the current time horizon. This prediction uses a correction that is computed in the following way: the states computed by the EKF at the delayed time horizon are compared with the predicted states for that horizon that have been buffered. The difference is used to estimate the bias of the IMU (gyroscope and accelerometer).
	\item It is capable of fusing a large range of different sensor types, and it is relatively easy to add new sensors (even non-linear).
	\item It can detect and report statistically significant inconsistencies in sensor data (useful for debugging).
	\item It estimates 3-axis accelerometer bias, which improves accuracy for vehicles that experience large attitude changes between flight phases.
	\item By combining attitude and position/velocity estimation, the attitude estimation benefits from all sensor measurements (it is improved if tuned correctly).
\end{itemize}


\begin{figure}[h!]
	\centering
	\includegraphics[width=1\textwidth]{./figures/estimation/horizontime.png}
	\caption{EKF2 strategy to take into account different time horizons of the sensors}
	\label{horizons}
\end{figure}


\subsubsection{The algorithm and its implementation}
We recall the Extended Kalman Filter (EKF) algorithm \cite[p. 54]{probarobotics}. Assume that the state transition and measurements are governed by the equations:
\begin{align*}
	\label{Eq:EKF}
	x_t &= g(u_t,x_{t-1}) + \epsilon_t \\
	z_t &= h(x_t) + \delta_t
\end{align*}
where $x_t$ is the state, $u_t$ the actuators, $z_t$ the measurements, $\epsilon_t$ and $\delta_t$ noises with covariances $R_t$ and $Q_t$, and $g$ and $h$ nonlinear functions.
The EKF algorithm is based on a linearization of $g$ and $h$, using their jacobians $G_t$ and $H_t$. Then a Kalman Filter is applied to the linearized system. The following algorithm computes an estimate $\mu_t$ and its variance $\Sigma_t$ from their previous values $\mu_{t-1}$ and $\Sigma_{t-1}$, the actuators and the measurements.
\clearpage
\begin{lstlisting}[mathescape=true,numbers=left]
Algorithm Extented_Kalman_filter($\mu_{t-1}$,$\Sigma_{t-1}$,$u_t$,$z_t$):
	$\overline{\mu_t} = g(u_t,\mu_{t-1})$
	$\overline{\Sigma_t} = G_t\Sigma_{t-1}G_t^T + R_t $
	$K_t = \overline{\Sigma_t}H_t^T(H_t\overline{\Sigma_t}H_t^T + Q_t)^{-1}$
	$\mu_t = \overline{\mu_t} + K_t(z_t - h(\overline{\mu_t}))$
	$\Sigma_t = (I - K_tH_t) \overline{\Sigma_t}$
	return $\mu_t$, $\Sigma_t$
\end{lstlisting}


The EKF2 estimator essentially uses this algorithm. Instead of treating all measurements as a single vector $z_t$, they are fused one by one: lines 4-6 are repeated for each sensor.
To improve precision the trapezoid method is used for integration.
The code to compute the jacobians $G_t$, $H_t$, the Kalman gain $K_t$ and the new covariance matrix $\Sigma_t$ is quite involved (367 lines of code for the prediction alone, line 3 of the above algorithm) because of the high dimension of the state and the non-linearity of the system. To produce this code without errors, symbolic computation in matlab was used (\prog{EKF/matlab/generated/Inertial Nav EKF}). This also allowed to optimize the code.
