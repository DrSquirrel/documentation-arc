This approach has two distinct steps: first we estimate the attitude via the Q estimator and then we estimate the position via a Kalman Filter (Local Position Estimator).

\subsubsection{Q estimator}
% topics:
% *	INs:
% 	* vehicle_odometry
% 	* vehicle_magnetometer
% 	* sensor_combined
% 	* vehicle_global_position
% 	* *att_pose_mocap*
% *	OUTs:
% 	* vehicle_attitude

The Q estimator is a simple quaternion based attitude estimator. It is a complementary filter. In the PX4 firmware this is implemented as a module and the code is located at \prog{Firmware/\penalty0 src/modules/attitude\_estimator\_q}. Its state consists of the attitude represented as a quaternion \prog{\_q} and a 3D vector \prog{\_gyro\_bias} to track the gyroscope bias.
\medskip

First, \prog{\_q} is initialized via a deterministic method. Then the main loop is iterated, which does 3 things:
\begin{enumerate}
	\setlength{\itemsep}{0em}
	\item compute the attitude change \prog{corr} 
	\item update the gyro bias \prog{\_gyro\_bias}
	\item compute the new value of \prog{\_q} from \prog{corr}
\end{enumerate}

\medskip
%---`corr` computation
In the first step of the main loop, the attitude change \prog{corr} is computed. It results of the sum of 2 components:
\begin{equation}
\label{Eq:corr}
\prog{corr} = \prog{rot\_speed} + \prog{correction}
\end{equation}
where \prog{rot\_speed} is the rotation speed measured by the gyroscope and corrected by the \prog{\_gyro\_bias}(prediction step), and where \prog{correction} is the correction according to the other sensors. This \prog{correction} is computed in the following way:
\begin{itemize}
	\item first the heading is obtained using the magnetometer (if it is not available, it is emulated using other sensors, in our case the MoCap). It is then compared with the current heading (\prog{\_q}) to give the \prog{\_mocap\_hdg\_err} which is used to compute a rotation about the vertical axis. It is possible to tune how much we trust this newly computed heading via the weights: \prog{\_w\_mag} (if using the magnetometer) or \prog{\_w\_ext\_hdg} (if using another external sensor such as the MoCap). 
	\item And then the accelerometer is used to determine the variation of the vertical direction. If an external measurement \prog{\_pos\_acc} of the acceleration is available (for instance from the GPS) then it is subtracted from the accelerometer measurement. In our case \prog{\_pos\_acc} is always 0 as we do not have GPS; the MoCap could be used instead but this is not implemented in the PX4. It is possible to tune the accelerometer influence thanks to the weight \prog{\_w\_accel}. In fact, this is the only time that the accelerometer is used in the Q estimator. 
\end{itemize}
%---`corr` computation

\medskip
%--- update the gyro bias
In the second step of the main loop, the gyroscope bias \prog{\_gyro\_bias} is updated. If in the first step, there was a need to apply a correction from the sensor measurement (\prog{correction} is not null), then it means the \prog{\_gyro\_bias} needs to be updated. It is possible to tune how much \prog{correction} influences the \prog{\_gyro\_bias} thanks to the weight \prog{\_w\_gyro\_bias}. From an implementation point of view, as this second step depends only on the \prog{correction} term (Eq~\ref{Eq:corr}), it is more practical to compute \prog{correction} first then update the bias and then add the first component \prog{rot\_speed} to obtain \prog{corr}.
%--- update the gyro bias

\medskip
%---the main loop correction application
In the third step of the main loop, the new value of \prog{\_q} is computed. First the derivative of \prog{\_q} is computed before being integrated and added to \prog{\_q}. This is an additive method so there is no guarantee that \prog{\_q} is a unit quaternion afterwards, so it needs to be normalized. Other methods use multiplication of \prog{\_q} by a corrective unit quaternion to avoid this.
% Additive correction of the quaternion: we add the `delta_q` to the  previous quaternion.
%---the main loop correction application



%% THE CODE
\clearpage
\begin{lstlisting}[language=C++,numbers=left]
// note: _q.conjugate_inversed turns a vector in local earth frame into a vector in body frame.

// MoCap
corr += _q.conjugate_inversed(Vector3f(0.0f, 0.0f, -mocap_hdg_err)) * _w_ext_hdg;

// Accel
Vector3f k = _q.conjugate_inversed(Vector3f(0.0f, 0.0f, 1.0f));
corr += (k % (_accel - _pos_acc).normalized()) * _w_accel; 
	// note: _accel - _pos_acc points toward the ground

// Gyro bias
_gyro_bias += corr * (_w_gyro_bias * dt);
for (int i = 0; i < 3; i++) {
	_gyro_bias(i) = math::constrain(_gyro_bias(i), -_bias_max, _bias_max);
}

_rates = _gyro + _gyro_bias;

// Feed forward gyro
corr += _rates;

// Apply correction to state
_q += _q.derivative1(corr) * dt;

// Normalize quaternion
_q.normalize();
\end{lstlisting}


Tuning this filter simply consists in adjusting the \prog{\_w\_*} weights.

%---------------------------------------------------------------

\subsubsection{Local Position Estimator (LPE)}
% x(+) = A * x(-) + B * u(+)
% y_i = C_i*x

% kalman filter

% E[xx'] = P
% E[uu'] = W
% E[y_iy_i'] = R_i

% prediction
% 	x(+|-) = A*x(-|-) + B*u(+)
% 	P(+|-) = A*P(-|-)*A' + B*W*B'

% correction
% 	x(+|+) =  x(+|-) + K_i * (y_i - H_i * x(+|-) )


% input:
%      ax, ay, az (acceleration NED)

% states:
%      px, py, pz , ( position NED, m)
%      vx, vy, vz ( vel NED, m/s),
%      bx, by, bz ( accel bias, m/s^2)
%      tz (terrain altitude, ASL, m)

% measurements:

%      sonar: pz (measured d*cos(phi)*cos(theta))

%      baro: pz

%      flow: vx, vy (flow is in body x, y frame)

%      gps: px, py, pz, vx, vy, vz (flow is in body x, y frame)

%      lidar: pz (actual measured d*cos(phi)*cos(theta))

%      vision: px, py, pz, vx, vy, vz

%      mocap: px, py, pz

The LPE estimator is a module of the PX4 (the source code is located at \prog{Firmware/src/\penalty0 modules/local\_position\_estimator}). It uses the Kalman Filter (KF) algorithm to process sensor measurements and provides an estimate of the following states:
\begin{itemize}
	\item Position at the IMU - North, East, Down (m)
	\item Velocity at the IMU - North, East , Down (m/s)
	\item IMU delta velocity bias estimates - X, Y, Z (m/s) (accelerometer bias)
	\item terrain altitude (m)
\end{itemize}
Attitude is not part of the state and it is treated as a constant. This allows a lot of simplifications and in particular it makes the system linear. So a standard Kalman Filter can be used.

Many sensors can be used: sonar, barometer, optical flow, GPS, lidar, vision, MoCap. However the algorithm does not take into account the different delays in the sensor data: it assumes that the data coming from the sensors are at the current date when processed.

The Kalman Filter is usually expressed in discrete setting \cite[p. 40]{probarobotics}. The LPE estimator uses a continuous time variant. The reason is that the interval between measurements may not be constant. Assume that the system is governed by the equations:
\begin{align}
	\label{Eq:KF}
	\dot x &= Ax + Bu \\
	y &= Cx
\end{align}

The estimate $\hat x$ and its variance $P$ are computed according to the following data flow \cite{kfbucy}, where the matrices $A$, $B$, $C$ are denoted $F$, $G$, $H$ respectively.
\begin{figure}[h!]
	\centering
	\includegraphics[width=1\textwidth]{./figures/estimation/KFBUCY.png}
	\caption{Continuous time Kalman-Bucy filter data flow}
	\label{FIG:KF}
\end{figure}

Here the state $x$ (a 10-dimensional vector) and the matrices $A$, $B$ are given blockwise by:
\begin{equation}
	\label{Eq:dynamics}
	x = \begin{bmatrix} 
		p \\
		v \\
		b \\
		h
	\end{bmatrix} \qquad
	 A = \begin{bmatrix}
		 0 & I_3 & 0  & 0 \\
		 0 & 0   & -R & 0 \\
		 0 & 0   & 0  & 0 \\
		 0 & 0   & 0  & 0
	\end{bmatrix}	\qquad
	B = \begin{bmatrix} 
		0 \\
		I_3 \\
		0 \\
		0
	\end{bmatrix} 
\end{equation}
where $p$ is the position, $v$ the velocity, $b$ the accelerometer bias (in the local frame), $h$ the terrain altitude, and $R$ is the rotation matrix describing the attitude, computed from the quaternion provided by the Q estimator. So the first equation of Eq\ref{Eq:KF} reduces to
\begin{align*}
	\dot p &= v \\
	\dot v &= u - Rb \\
	\dot b &= 0 \\
	\dot h &= 0
\end{align*}
The bias has to be converted to the global frame before being subtracted from the measured acceleration $u$. In the absence of other measurements $b$ and $h$ are not changed.

% \begin{lstlisting}[mathescape=true,numbers=left]
% Algorithm Kalman_filter($\mu_{t-1}$,$\Sigma_{t-1}$,$u_t$,$z_t$):
% 	$\bar{\mu_t} = A_t\mu_{t-1} + B_tu_t$
% 	$\bar{\Sigma_t} = A_t\Sigma_{t-1}A_t^T + R_t$
% 	$K_t = \bar{\Sigma_t}C_t^T(C_t\bar{\Sigma_t}C_t^T + Q_t)^{-1}$
% 	$\mu_t = \bar{\mu_t} + K_t(z_t - C_t\bar{\mu_t})$
% 	$\Sigma_t = (I - K_tC_t) \bar{\Sigma_t}$
% 	return $\mu_t$, $\Sigma_t$
% \end{lstlisting}


We present now a fraction of the code of the LPE module. The actual code contains many tests to check the validity of the measurements and of the estimations. Also, bias is limited to a maximum value (saturation). We have removed these lines for legibility.

%% PREDICT CODE
The part of the code dealing with the prediction is:
\begin{lstlisting}[language=C++,numbers=left]
// retrieve attitude from Q estimator
matrix::Quatf q(&_sub_att.get().q[0]);
_eul = matrix::Euler<float>(q);
_R_att = matrix::Dcm<float>(q);

// get acceleration
// note, bias is not removed here as it is removed in dynamics function
Vector3f a(_sub_sensor.get().accelerometer_m_s2);
_u = _R_att * a;
_u(U_az) += CONSTANTS_ONE_G;	// add g

// update state space based on new states
updateSSStates(); // compute the matrix _A using the rotation _R_att

// continuous time Kalman filter prediction
// integrate Runge Kutta 4th order
float h = getDt();
Vector<float, n_x> k1, k2, k3, k4;
k1 = dynamics(0, _x, _u);
k2 = dynamics(h / 2, _x + k1 * h / 2, _u);
k3 = dynamics(h / 2, _x + k2 * h / 2, _u);
k4 = dynamics(h, _x + k3 * h, _u);
Vector<float, n_x> dx = (k1 + k2 * 2 + k3 * 2 + k4) * (h / 6);

// propagate
_x += dx;
Matrix<float, n_x, n_x> dP = (_A * _P + _P * _A.transpose() + _B * _R * _B.transpose() + _Q) * getDt();
_P += dP;
_xLowPass.update(_x);
\end{lstlisting}

At lines 19-22, the function \prog{dynamics(t,x,u)} simply computes \prog{\_A * x + \_B * u}. Note that the estimate is not published directly but through a low pass filter (line 29) to eliminate the high frequency noise.

\clearpage
The update is done for each sensor. As the code is similar we show only the update for the MoCap measurements. Here the $C$ matrix is given by:
\begin{equation}
	C =\begin{bmatrix} I_3 & 0 & 0 & 0 \end{bmatrix} 
\end{equation}
%% TODO UPDATE CODE
\begin{lstlisting}[language=C++,numbers=left]
// measure
Vector<float, n_y_mocap> y;
if (mocapMeasure(y) != OK) { return; }

// mocap measurement matrix, measures position
Matrix<float, n_y_mocap, n_x> C;
C.setZero();
C(Y_mocap_x, X_x) = 1;
C(Y_mocap_y, X_y) = 1;
C(Y_mocap_z, X_z) = 1;

// noise matrix
Matrix<float, n_y_mocap, n_y_mocap> R;
R.setZero();
float mocap_p_var = _mocap_p_stddev.get() * _mocap_p_stddev.get();
R(Y_mocap_x, Y_mocap_x) = mocap_p_var;
R(Y_mocap_y, Y_mocap_y) = mocap_p_var;
R(Y_mocap_z, Y_mocap_z) = mocap_p_var;

// residual
Vector<float, n_y_mocap> r = y - C * _x;
// residual covariance
Matrix<float, n_y_mocap, n_y_mocap> S = C * _P * C.transpose() + R;
// residual covariance, (inverse)
Matrix<float, n_y_mocap, n_y_mocap> S_I = inv<float, n_y_mocap>(S);

// Kalman filter correction 
Matrix<float, n_x, n_y_mocap> K = _P * C.transpose() * S_I;
Vector<float, n_x> dx = K * r;
_x += dx;
_P -= K * C * _P;
\end{lstlisting}
