\subsection{Motivation}
In the drone choir project, we first used the Q+LPE estimator, and it gave satisfactory results. Then we moved to EKF2 as it became the recommended pose estimator for the PX4, and it also gave satisfactory results. But we felt that a more rigorous comparison of the two estimators would be useful. This was not easy to do with the drones themselves, as they are a complicated system with several interfering components, and it is very difficult to reproduce twice exactly the same conditions. Therefore we designed the experiments presented in this section.

We also wanted to understand better the behaviour of the estimators in degraded conditions, which occur often but not in a predictable way. Indeed, we observed the following conditions:
\begin{itemize}
\item MoCap freeze: The MoCap may lose track of the drone, for instance in some corners of the stage that are not well covered by cameras, and when this happens it continues to send the last seen position as if the drone was still there.
\item MoCap confusion: When many objects are present the MoCap sometimes wrongly identifies them and sends the wrong positions.
\item Network errors: For short intervals, MoCap data does not reach the drones.
\item Network loss: MoCap data stops completely.
\end{itemize}

In such conditions, the drone relies only on its pose estimator to get its position. It may decide to take safeguard measures, such as stopping (hovering) or landing, but even to hover or land precisely a drone needs a good pose estimation, long enough to complete the landing. A drift of the position estimate during hovering will likely result in a crash against a wall or ceiling; a wrong attitude estimate will have bad consequences even faster. Therefore we designed (and implemented) a way to simulate some of those conditions.

Due to time constraints, to the lack of availability of the MoCap room, and to the priority given to the project itself, we were not able to actually conduct those experiments.

\subsection{The setup}
For these experiments, we use a single drone but it does not fly autonomously, instead it is moved across the MoCap area, either hand held or on a rolling cart, following a prescribed trajectory.

The drone is equipped with a second PX4 flight controller. Both flight controllers are instructed to believe that they are flying (for this many pre-flight checks need to be deactivated or they will refuse to go into flying mode), and they are setup in exactly the same way, except for the setting that we want to evaluate. Both are connected to the onboard computer that records all relevant data: IMU measurements, estimates (along with their variances) of the 3 coordinates and of the attitude quaternion, along with filtered and unfiltered (see below) MoCap data.

To simulate degraded conditions, we modified the \verb+mocap_optitrack+ node (a program provided by the MoCap vendor that converts the flow coming from the MoCap software into ROS topics) so that it filters the data in one of the following ways:
\begin{itemize}
\item MoCap functions normally
\item MoCap only works when the drone is in a pre-defined area, then stops
\item MoCap only works when the drone is in a pre-defined area, then freezes (repeats the last position forever)
\item MoCap is subsampled at a slow frequency
\end{itemize}
In any case, the unfiltered data is also transmitted in another topic, to serve as {\em ground truth}.

\subsection{The experiments}
In a first series of experiments, one flight controller is configured to use the Q+LPE estimator, the other one to use the EKF2 estimator. Apart from that, their settings are identical.

The drone is moved, first along a straight line, then on a circle, then on a horizontal trajectory with sharp turns, then on a 3D trajectory (note however that since the two PX4 are attached one above the other but not at exactly the same position, any trajectory that involves non-zero pitch or roll will result in slightly different trajectories for the two PX4).

During the experiments, the onboard computer logs the pose as given by the MoCap, the estimates of each controller, and their variances. This allows to compare the controllers in a normal flight situation.

In the second series of experiments, again with the PX4 setup with two different estimators, we simulate various MoCap or network incidents. This allows to check how well the estimators fill the gap during temporary MoCap losses, how they react when the MoCap freezes, and for how long they provide a sensible pose after a permanent loss.

In the third series of experiments, we setup the two PX4 with the same estimator, but with different settings, e.g. the weights in the Q+LPE estimator, or the variances and delays in the EKF2 estimator. Then we move the drone with a normal or degraded MoCap. This allows to tune those parameters (which we mostly had left to their default values).

\subsection{Discussion}

We are particularly interested in observing the behaviour of the variance of the estimates. Variance is important because it allows to know the quality of the estimate, and it could be used for instance by the onboard computer to detect that something is wrong (maybe a MoCap confusion) and decide to abort the flight. Currently, it only detects loss of data, not wrong data.

Unfortunately the current implementation of the LPE does not publish the variance via mavlink. So we need to collect it directly on the PX4. A novel solution would be to use the ROS2 environment since it allows to communicate with the PX4 directly through uORB topics without using mavlink \cite{ROS2}.
%(cite why and where to find).
However the development and the integration of ROS2 to the PX4 project is not advanced enough. Moreover the MoCap interface (\verb+mocap_optitrack+) is not implemented in ROS2 yet. This would be an interesting work in the future.

Another benefit of the experiments would be to know how long one can safely trust the estimate after a MoCap loss. This would allow the onboard computer to wait some time (maybe a few seconds) before aborting the flight and landing, in the hope that the loss is only temporary and that it can resume normal operation after this wait without too much risk. (However, it is not reasonable to continue to fly for too long after a network loss, even if the drone's own position is correctly estimated, because it cannot know the other drones' positions and would risk crashing into them.)

