\emph{Scene 2} is the scene that has evolved the most during this project. It was never performed in front of an audience. The script has been tested in part but should still be considered as experimental.

This scene follows immediately Scene 1. Glauce is on the stage with the gifts (the drones) from Medea given by Neris.

The main thread of the choreography is made of two parts, the main program properly and the procedure \prog{continue}.

\begin{lstlisting}
# ----------
# SCENE 2
# ----------

music play 2

## Drones are carried one by one, in order, to auto-takeoff area
## When drone 5 is flying, proceed
## maximum duration: 5 minutes
uav 6 init
uav 7 init
uav 8 init
uav 9 init
uav 10 init
when takeoffArea_10 continue
sleep 300
\end{lstlisting}

A separate thread is launched for each drone, running procedure \prog{init}, and a handler is set up so that the main thread continues with procedure \prog{continue} after the last drone is launched. Finally, it waits for 5~minutes: normally, this is more than needed to launch all 5 drones, so the \prog{sleep} instruction never gets completed. However, if something goes wrong, after this time all drones are landed and execution stops.

\begin{lstlisting}
## The drones wait for drone 5 to take position,
### then start to fall randomly
## duration: 150 s
def continue
sleep 8

set interval 0.5

uav 6 randomfall
uav 7 randomfall
uav 8 randomfall
uav 9 randomfall
uav 10 randomfall

sleep 30
set interval .4
sleep 30
set interval .3
sleep 30
set interval .2
sleep 30
set interval .1
sleep 30


## The drones remain still for a moment, then land
## duration: 5 s
uav 6 finishing
uav 7 finishing
uav 8 finishing
uav 9 finishing
uav 10 finishing
sleep 5

## End of the scene
\end{lstlisting}

The \prog{continue} procedure contains the actual scene. Its execution starts when the last drone enters the take-off area, so it is necessary to wait a short time to let this drone reach its base position. At this moment, all drone threads are instructed to execute procedure \prog{randomfall}, with variable \prog{interval} set to $0.5\,\mathrm{s}$. Then, every 30~seconds, \prog{interval} is reduced, resulting in a progressive acceleration of the rythm of the falls. This lasts for 2~minutes and 30~seconds. Then all drone threads are instructed to execute procedure \prog{finishing}, which ends whatever the drones were doing and lands them within 5~seconds. After 5~seconds, execution stops and all drones still flying are automatically landed.

\begin{lstlisting}
#--------------------
# Functions
#--------------------

## init: a drone gets ready to take off
## duration: 0 s
def init
when takeoffArea fly

## fly: when in take off area, a drone plays music,
## takes off and goes to its base position
## duration: 8 s
def fly
when isReadyToGo init
music play 3
mode 1
upto 2.5
sleep 1
offboard
sleep 2
uav 6 point -1.8 1.2 2.5
uav 7 point -1.6 2.8 2.5
uav 8 point 0 1.2 2.5
uav 9 point 1.5 1.2 2.5
uav 10 point 1.5 2.8 2.5
sleep 3
mode 6
look public
predrop
sleep 2
\end{lstlisting}

The \prog{init} procedure does nothing but setting up a handler for the \prog{takeoffArea} event, which is triggered when the drone is carried to the take-off area.

This handler, the \prog{fly} procedure, makes the drone start to play music, wait one second (this is a safety measure: music acts as a warning that the drone is going to take off), take off from the hands of the attendant and raise to its desired altitude ($2.5\,\mathrm{m}$), wait again 2~seconds, then take its assigned position (which depends on the drone number). After 3 more seconds,
the drone looks toward the audience and switches to mode 6, which enforces its altitude but allows it to drift in an horizontal plane, so that it can move away from other drones when it is repulsed by them. Finally, it also sets the \prog{predrop} condition, which creates a repulsive cylinder under the drone so that other drones will not fly there.

The \prog{fly} procedure also sets a handler for the \prog{isReadyToGo} event: this is in case some unexpected event (such as a temporary MoCap or network failure) causes the drone to land in emergency. If this happens, and then the failure condition disappears, then the drone will be ready to take off again if the attendant picks it up and brings it to the take-off area again. 

\begin{lstlisting}
## randomfall: a drone falls after a random wait
## duration: variable, average 10 s
def randomfall
when isReadyToGo restart
repeat 0
  random 20 fall
  sleep \$interval
end
\end{lstlisting}

The \prog{randomfall} procedure is run by the drone when it is flying at its high level and is allowed to drop at any moment.After setting up a handler for \prog{isReadyToGo} (in case of a failure, see above, but with a different handler), it enters an infinite loop, the purpose of which is to simulate an exponential random variable. At each iteration of the loop, it may jump out to procedure \prog{fall} with probability $1/20$. The expectancy of the number of iterations is thus $20$, which means that, at the beginning of the scene (when \prog{interval} is $0.5\,\mathrm{s}$), the drone will fall after 10~seconds in average, but the wait can be any time between $0$ and infinity: there is no way to predict when it will happen. Later in the scene, variable \prog{interval} will be decreased and thus the average time will also decrease, to 2~seconds at the end.

\begin{lstlisting}
## fall: a drone falls, then raises somewhat after a random wait
## duration: variable, minimum 8 s, average 13.2 s
def fall
when cantDrop canceldrop
music play 3
sleep 4
music play 2
upto 1
drop
sleep 4
repeat 0
  random 50 level1
  random 49 level2
  random 48 level3
  random 47 level4
  random 46 level5
  sleep \$interval
end
\end{lstlisting}

The \prog{fall} procedure makes a drone fall, but not immediately. First it changes its music, and sets a handler for the \prog{cantDrop} event, which happens when it is impossible to drop (for instance, if the dancer is too close). After 4~seconds, it changes its music again, sets a new altitude goal at $1\,\mathrm{m}$ (which will be used after the drop) and drops. Another wait of 4~seconds allows the drone to stabilize. Then it enters again an infinite loop similar to the one in \prog{randomfall}, with the difference that here several jumps are possible. The probabilities are chosen in such a way that at each iteration, all five jumps are equiprobable, with a probability of $1/50$ each, and there is a probability of $9/10$ to continue the loop. Indeed, procedure \prog{level1} is called with probability $1/50$; procedure \prog{level2} is called with probability $49/50\times1/49$; procedure \prog{level3} is called with probability $49/50\times48/49\times1/48$; and so on. The result is that the time that the drone will stay at the lower level is unpredictable (with an expectancy of $5\,\mathrm{s}$ at the beginning of the scene, which then decreases to $1\,\mathrm{s}$),
and the level to which it will raise is also unpredictable.

\begin{lstlisting}
## level1: a drone raises to 1.3 m, then raises again after a random wait
## duration: variable, minimum 1 s, average 6.2 s
def level1
music stop
upto 1.3
sleep 1
music play 3
repeat 0
  random 40 level2
  random 39 level3
  random 38 level4
  random 37 level5
  sleep \$interval
end

## level2: a drone raises to 1.6 m, then raises again after a random wait
## duration: variable, minimum 1 s, average 6.2 s
def level2
music stop
upto 1.6
sleep 1
music play 3
repeat 0
  random 30 level3
  random 29 level4
  random 28 level5
  sleep \$interval
end

## level3: a drone raises to 1.9 m, then raises again after a random wait
## duration: variable, minimum 1 s, average 6.1 s
def level3
music stop
upto 1.9
sleep 1
music play 3
repeat 0
  random 20 level4
  random 19 level5
  sleep \$interval
end

## level4: a drone raises to 2.2 m, then raises again after a random wait
## duration: variable, minimum 1 s, average 6 s
def level4
music stop
upto 2.2
sleep 1
music play 3
repeat 0
  random 10 level5
  sleep \$interval
end

## level5: a drone raises to 2.5 m, then resumes falling randomly
## duration: 2 s
def level5
upto 2.5
sleep 1
music stop
sleep 1
randomfall
\end{lstlisting}

Procedures \prog{level1} to \prog{level4} are all similar: the drone raises to a higher altitude (each level being separated by $30\,\mathrm{cm}$), stops its music then plays a different track after one second, and enter an infinite loop containing random jumps to higher levels. Procedure \prog{level5} is different: the drone reaches its highest altitude, and after stopping the music and waiting 2~seconds, it returns to procedure \prog{randomfall}.

\begin{lstlisting}
## canceldrop: if the drone cannot drop, abort
## and recenter before resuming dronefall
## duration: 2 s
def canceldrop
music stop
mode 1
point 0 2 2.5
sleep 0.5
mode 6
sleep 1
upto 2.5
mode 1
sleep 1
mode 6
randomfall
\end{lstlisting}

Procedure \prog{canceldrop} is called as an event handler when somthing prevents the drop. In this case, music is stopped, a goal in the center of the stage and at altitude $2.5\,\mathrm{m}$ is given, then the drone is switched to mode 1 during half a second. This will give it an impulsion that will reposition it closer to the center (maybe at the center if there is no drone already there). Repulsion between drones will ensure that they remain at a safe distance from each other. After that, it is switched to mode 6 again. The operation is repeated, now with a goal at the current position. Finally, execution of \prog{randomfall} is restarted.

\begin{lstlisting}
## finishing: a drone returns to top level, waits, then lands
## duration: 5 s
def finishing
music stop
mode 1
upto 2.5
sleep 5
land
\end{lstlisting}

Procedure \prog{finishing} is run at the end of the scene.
When it starts, the drone may be at any level. Music is stopped,
the drone returns one last time to the top level, then lands.

\begin{lstlisting}
# restarting
def restart
upto 2.5
offboard
sleep 4
mode 6
randomfall
\end{lstlisting}

Procedure \prog{restart} is the handler for event \prog{isReadyToGo} once the drone has succesfully taken off, it is set up at the first execution of procedure \prog{randomfall} by that drone. When an emergency landing occurs, and the drone becomes available again, then it takes off by itself, raises to the high level, waits for 4~seconds to stabilize, switches to mode 6 and resumes execution of procedure \prog{randomfall}.
