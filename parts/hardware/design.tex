% \begin{itemize}
% 	\item constraints
% 	\item the design (schematics + CAD model public avalaible)
% 	\item part list and where to get them
% 	\item building process (steps)
% 	\item characteristics (in a table, flight time and so on) %%TODO Measures!
% \end{itemize}

%---- CONSTRAINTS
\subsection{Constraints}
For the design of the drones, we had to meet several constraints. A drone should:
\begin{itemize}
	\item \textbf{carry a speaker}. This means that the drone needs to be able to fly with some additional weight and the placement of the speaker should not hinder the air circulation/aerodynamics of the drone. Additionally we need to be able to hear the sound coming from those speakers. Need to have the drones as quiet as it is possible.
	\item \textbf{fly more than 4 mins}. To do so, we need a powerful battery.
	\item be \textbf{safe}, in particular prevent the propellers from hurting the dancer if there is a crash (propeller protections).
	\item be able to \textbf{communicate wirelessly} with a main ground station computer.
	\item have an easy way to put \textbf{MoCap markers} on it.
	\item be \textbf{practical to work with}. Especially having the possibility to power the onboard computer without powering the motors.
	\item be able to \textbf{control several drones at the same time} in order to coordinate their trajectories.
\end{itemize}
We had to do many trade-offs to meet all those constraints.
%---- CONSTRAINTS

%---- PARTS
\subsection{The parts}
To design the drones, first we chose the parts that were commercially available and then we 3D modeled the remaining parts such as the extended frame, speaker and propeller protections.
% -------------------------------------------------------------------------------------------------
% | parts (name generic in drone) | chosen part (name)        | weight | price | current avability (url) |
% -------------------------------------------------------------------------------------------------
% | Frame							| EMAX - Nighthawk X6	|
% -------------------------------------------------------------------------------------------------
% | Flight Controller				| Pixracer				|
% -------------------------------------------------------------------------------------------------
% | ESC							| DYS XS30A BLHeli\_S Firmware 3-6S 8BB2
% -------------------------------------------------------------------------------------------------
% | PDB							| Nighthawk X PDB
% -------------------------------------------------------------------------------------------------
% | Motors						| DYS SE2205 PRO 2550KV
% -------------------------------------------------------------------------------------------------
% | Propellers					| DAL 6x4.5 Bullnose Propeller v2
% -------------------------------------------------------------------------------------------------
% | Battery						| 
% -------------------------------------------------------------------------------------------------
% | Power Module 					|
% -------------------------------------------------------------------------------------------------
% | Y-PWR							|
% -------------------------------------------------------------------------------------------------
% | On board computer				|
% -------------------------------------------------------------------------------------------------

Let us look at all the component parts we need and their functions. It is important to note that all the components have been selected so that they are quickly available. Therefore we have prioritised components available in Swedish stores. However the availability of some components changes with time. We have learnt that keeping the same components for all the drones is crucial as it makes maintenance and debugging easier.

\newpage
\bgroup\hbadness 6000
\subsubsection{The frame}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
    \includegraphics[width=0.48\textwidth]{./figures/hardware/frame.jpg}
	\caption{\emph{Nighthawk-X6\/} from EMAX}
	\label{Fig:frame}
\end{wrapfigure}
The frame is the first part we selected. We chose the \emph{Nighthawk-X6\/} from EMAX (see Figure~\ref{Fig:frame}).

In fact, there are three types of quadrotor frames: the \emph{X configuration}, the \emph{+ configuration\/} and the \emph{H configuration\/} (see Figure~\ref{Fig:frame-config}). Only the orientation of the drone differs between the \emph{X\/} and the \emph{+ configurations}. We picked the \emph{X configuration\/} as it seems the most common among small drones since for a fixed size it allows bigger propellers.

The \emph{Nighthawk-X6\/} is made of $3\,\mathrm{mm}$ thick carbon fiber which is both sturdy and light. All the components are to be arranged toward the center, making it easier to place the center of gravity of the drone in its center. It comes with replaceable arms.

The dimensions of a frame are measured according to its wheelbase (i.e., the diagonal distance between two motor axes). The dimension of the frame restricts which type of motors and propellers we can use. We choose a $240\,\mathrm{mm}$ frame because we know we are going to mount a speaker and an onboard computer on it.

\medskip
\begin{center}
\begin{tabular}{|l|l|}
    \hline
    Name & EMAX - Nighthawk X6\\
    \hline
	Weight & $95\,\mathrm{g}$\\
    \hline
	Size & $240\,\mathrm{mm}$\\
    \hline
	Material & $3\,\mathrm{mm}$ CF\\
    \hline
    Compatible Motors & MT22XX\\
    \hline
	Compatible Props & $6''$ max ($152.4\,\mathrm{mm}$)\\
    \hline
	Available at & \prog{https://www.flyingtech.co.uk}\\
    \hline
\end{tabular}
\end{center}

\begin{figure}[b]
    \centering
    \includegraphics[width=0.7\textwidth]{./figures/hardware/frame-config.png}
    \caption{Three different quadrotor frame configurations}
    \label{Fig:frame-config}
\end{figure}

%-----------------
\clearpage
\subsubsection{The motors}
\label{part:motor}

\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/motors.jpg}
	\caption{\emph{DYS SE2205 PRO 2550KV\/} Brushless Motors}
	\label{Fig:motors}
\end{wrapfigure}
Due to our choice of the frame, we were constrained to choose MT22XX motors (motors having a stator with a $22\,\mathrm{mm}$ diameter). We chose the \emph{DYS SE2205 PRO 2550KV\/} Brushless Motors (see Figure~\ref{Fig:motors}). Two of them have to rotate clockwise and the other two counterclockwise. And it is important to know which one is clockwise or counterclockwise before assembling the drones (check on Figure~\ref{Fig:rotationMotors}).

%% TODO missing figure Fig:rotationMotors

\paragraph{Brushless vs DC Motors} For drones, we use brushless motors instead of classic DC motors (see Figure~\ref{Fig:motor-type}). In a brushless motor the permanent magnets are on the rotor (rotating part of the motor) and the electromagnets are on the stator (immobile part). Then it requires an Electronic Speed Controller (ESC) to take care of switching the voltage of these electromagnets to make the rotor spin. Such motor has several advantages: no more brushes that can be worn-out or need to be cleaned, no more frictions. A brushless motor is more efficient and allows a more precise speed control than a regular brush motor. Also the electromagnets on the stator are easy to cool.
\begin{figure}[b]
	\centering
	\includegraphics[width=0.5\textwidth]{./figures/hardware/brushed-brushless.jpg}
	\caption{Brushed DC motor vs brushless DC motor}
	\label{Fig:motor-type}
\end{figure}

\paragraph{Characteristics} The motors are characterized by their:
\begin{itemize}
	\item \textbf{size}. They are usually named according to their size which is given by the diameter of the stator and its length. For instance the motor SE2205 has a stator with a $22\,\mathrm{mm}$ diameter and it is $5\,\mathrm{mm}$ tall. 
	\item \textbf{speed of revolution} Next to their name comes their speed given by the \emph{$\Kv$ Rating\/} coefficient. In fact, the speed is measured by the number of revolutions per minute (rpm) the motor will spin at full throttle given a voltage. And specifically $$\textrm{Number of revolutions per minute} = \Kv \times \textrm{voltage}$$ In our case at $11.1\,\mathrm{V}$, at full throttle our motors can speed at up to 28305 revolutions per minute ($\sim 471$ revolutions per second)! The thrust generated by the motors depends on its \emph{$\Kv$ Rating\/}, the type of propeller it is associated with and the quantity of power it can instantaneously draw from the ESC and the battery. Indeed, an increase in the thrust implies a higher current incoming from the battery. In physics we usually measure thrust in Newton ($\mathrm{N}$, i.e. $\mathrm{kg}\cdot\mathrm{m}\cdot\mathrm{s}^{-2}$) but in drone construction it is expressed in grammes ($\mathrm{g}$). If our quadrotor weights $2\,\mathrm{kg}$ (which is less than the final weight of our drone) then each motor needs to produce $500\,\mathrm{g}$ of thrust to make it takeoff. On the technical data sheet (Figure~\ref{Fig:motor-tech}) we can see that with those motors, if our drone is less than $2\,\mathrm{kg}$, it will have no problem to takeoff.
	\item \textbf{efficiency} Efficiency of the motor is measured by the power coming out divided by the power coming in. Basically the efficiency measures the percentage of the incoming power transformed into motion. The more efficient a motor is, the longer the drone will fly. And as a rule of thumb anything above $7\,\mathrm{g}\mathrm{W}^{-1}$ is considered good.
\end{itemize}
\begin{figure}[hp]
	\centering
	\includegraphics[width=0.9\textwidth]{./figures/hardware/motor-technical.png}
	\caption{\emph{DYS SE2205 PRO 2550KV\/} specs}
	\label{Fig:motor-tech}
\end{figure}

\medskip
Figure~\ref{Fig:motor-tech} given by the manufacturer gathers the specification of this motor.


% \paragraph{Thrust requirements.} The drone needs to be able to take off when its throttle is around 50\%. Let the total weight of our drone be $M_{total}$ then the thrust of one motor at 50\% throttle ($T_{MOT,    50}$): $$T_{MOT,50} = \frac{M_{total}}{4}$$ And, as we want a security margin, we add 20\%:
% \begin{equation}
%     \label{Eq:thrust}
%     T_{MOT,50} = \frac{M_{total}*1.2}{4} = M_{total}*0.3
% \end{equation}
% However, we realize that the weight contribution in $M_{total}$ is not even: the weight of the battery is by far the heaviest. Our security margin aims to take into account the additional soldering and wires. So  it makes no sense to take into account the weight of the battery into our security margin. That is why, in order to make our design more efficient, we decide to add the 20\% only to $M_{total} - M_{BAT}$. Our      formula becomes:
% \begin{equation}
%     \label{Eq:improvethrust}
%     T_{MOT,50} = \frac{(M_{total}-M_{BAT})*1.2 + M_{BAT}}{4} = M_{total}*0.3 - 0.05*M_{BAT}
% \end{equation}
% In our setup $M_{total} = 1173\,\mathrm{g}$ and $M_{BAT} = 529\,\mathrm{g}$. Thus $$T_{MOT,50} = 325.4\,\mathrm{g}$$
% \paragraph{Our choice.} In order to meet our thrust requirements, we choose the TigerMotor MT2212 750KV motors.

\begin{center}
\begin{tabular}{|l|l|}
    \hline
    Name & DYS SE2205 PRO 2550KV\\
    \hline
	Weight & $29\,\mathrm{g}$\\
    \hline
	Motor dimensions & $27.7\times19.0\,\mathrm{mm}$ (diameter $\times$ height)\\
    \hline
	Stator Diameter & $22\,\mathrm{mm}$\\
    \hline
	Stator Length & $5\,\mathrm{mm}$\\
    \hline
	$\Kv$ & $2550\,\mathrm{rpm}/\mathrm{V}$\\
    \hline
	Max. Continuous Current (A) & $27.8\,\mathrm{A}$ \\
    \hline
	Available at & \prog{https://www.flyingtech.co.uk}\\
    \hline
\end{tabular}
\end{center}


%-------------------------------
\clearpage
\subsubsection{The flight controller}
\label{build:px4}
\begin{wrapfigure}{L}{0.5\textwidth}
	% \vspace{-40pt}
	\centering
    \includegraphics[width=0.48\textwidth]{./figures/hardware/FC.jpg}
    \caption{Our flight controller: Pixracer}
    \label{Fig:pixracer}
\end{wrapfigure}
A flight controller is essentially a programmable microcontroller with specific sensors and connectors onboard. Paired with an autopilot firmware, it keeps our drone stable by dealing with the motor controllers. We chose the \emph{Pixracer\/} flight controller (see Figure~\ref{Fig:pixracer}) with the open source \emph{PX4\/} Firmware \cite{PX4}.

The \emph{Pixracer\/} is small and can be used either in a racing drone or in an autopilot stack system (what we intend to do). In a flight controller the sensors are the most important part since together with the firmware they perform operations that keep the drone level and stable in the air. The \emph{Pixracer\/} includes:
\begin{itemize}
	\item a \emph{ICM-20608-G\/} 6-axis motion tracking device (3-axis accelerometer, 3-axis gyroscope) 
	\item a \emph{MPU-9250\/} 9-axis motion tracking device (3-axis gyroscope, 3-axis accelerometer, 3-axis magnetometer)
	\item a \emph{MEAS MS5611\/} barometer 
	\item a \emph{Honeywell HMC5983\/} magnetometer (temperature compensated) 
\end{itemize}
For more detail on the use of those sensors go to Chapter~\ref{chapter:estimation}.

The \emph{Pixracer\/} also comes with a wifi module. It is meant to be used for wifi telemetry and firmware update and cannot be used for calibrating the sensors or receiving control commands. Because we will have an onboard computer, we decide to leave this component aside.

\begin{center}
\begin{tabular}{|l|l|}
    \hline
    Name & Pixracer\\
    \hline
	Weight & $10.54\,\mathrm{g}$\\
    \hline
	Size & $36\times36\,\mathrm{mm}$\\
    \hline
    Software & PX4\\
    \hline
	Available at & \prog{https://www.flyingtech.co.uk}\\
    \hline
\end{tabular}
\end{center}

%---------------------
\clearpage
\subsubsection{The propellers}
\label{part:props}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/props.jpg}
	\vskip2\baselineskip
	\caption{\emph{DAL TJ6045 3-Blades $6\times4.5$\/} Bullnose propellers}
	\label{Fig:props}
\end{wrapfigure}

We choose the \emph{DAL TJ6045 3-Blades $6\times4.5$\/} propellers (see Figure~\ref{Fig:props}). 

There are several types of propellers depending on their material, their dimensions, their pitch, their number of blades and their shape.

The material used to make the props influences the flight characteristics but it will also influence its potential to hurt and wound a human operator. However, we should not forget about safety. That is why we chose them to be in plastic instead of carbon fiber.

\medskip
The propellers are measured by their diameter in inches. For instance, $6''$ prop ($15.24\,\mathrm{cm}$) correspond to approximately $7.5\,\mathrm{cm}$ blade.

The pitch of a propeller is measured as a distance (usually in inches) instead of degree. It corresponds to the vertical distance a horizontal propeller would move after one revolution if it didn't slip.

\medskip
The difference between a two-blade propeller and a three-blade one is in the thrust produced for a fix material, dimension and pitch. The three-blade propeller can reach higher thrust, but then they draw more current making the drone consume more power. Because our drone will be quite heavy (speakers and many propeller protections), we had to choose three-blade propellers.

\medskip
Also, some propellers have a shape called ``bullnose''. It means that the end of the blade is larger than the normal ones.

\bigskip

\begin{center}
\begin{tabular}{|l|l|}
    \hline
    Name & DAL TJ6045 Bullnose Propeller\\
    \hline
	Weight & $3.8\,\mathrm{g}$\\
    \hline
    Material & Plastic \\
    \hline
    Number of blades & 3\\
    \hline
    Shape & Bullnose\\
    \hline
	Diameter & $6''$ ($15.24\,\mathrm{cm}$)\\
    \hline
	Pitch & $4.5''$\\
    \hline
	Available at & \prog{https://www.flyingtech.co.uk}\\
    \hline
\end{tabular}
\end{center}

%-----------------------------
\clearpage
\subsubsection{The ESC}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/ESC.jpg}
	\caption{\emph{DYS XS30A\/} ESCs}
	\label{Fig:esc}
\end{wrapfigure}
We choose the \emph{DYS XS30A\/} ESCs for our drone (see Figure~\ref{Fig:esc}).

An ESC (Electronic Speed Controller) controls the speed of a brushless motor. It has three sets of wires: one set to connect to the battery, one to connect to the radio receiver or the flight controller and another one to connect to the motor. It receives an input ppm signal (pulse position modulation) from the receiver (or flight controller) which indicates how much power it should send to the motor. A brushless ESC has three wires to connect to the motor but only two are energized at any given time. The wire that is not energized tells the ESC in which direction and how fast the motor spins by generating a small voltage proportional to the speed of the motor. That way the ESC knows how to adjust the charge of the electromagnets to follow the speed instruction from the receiver or the flight controller.


The ESC must be chosen so that it can handle the maximum current that the motor can draw. That is why the ESCs are rated for a maximum current. In our case, the maximum continuous current that can be drawn from the motor is $27.8\,\mathrm{A}$, so no problem for our $30\,\mathrm{A}$ ESCs.

\medskip
\begin{center}
\makebox[0pt]{
\begin{tabular}{|l|l|}
    \hline
    Name & DYS XS30A BLHeli\_S Firmware 3-6S 8BB2 ESC for Multicopter\\
    \hline
	Weight & $8.6\,\mathrm{g}$\\
    \hline
	Size & $45 \times 16 \times 6\,\mathrm{mm}$\\
    \hline
	Max. Continuous Current & $30\,\mathrm{A}$\\
    \hline
    Firmware & BLHeli\\
    \hline
	Available at & \prog{https://www.flyingtech.co.uk}\\
    \hline
\end{tabular}
}
\end{center}

%------------
\clearpage
\subsubsection{The onboard computer}

\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/rasp-pi.jpg}
	\caption{Raspberry Pi model B type 3}
	\label{Fig:rasp}
\end{wrapfigure}
We choose the Raspberry Pi model B type 3 as our onboard computer (see Figure~\ref{Fig:rasp}). With its 1.2 GHz 64-bit quad-core ARM Cortex-A53 CPU and its 1GB SDRAM memory, it allows us to run our robotic software platform \prog{ROS} and to control the music playlist for each drone. It comes with Wifi included but to increase the connection range, the bandwidth and the speed we decided to use an external wifi dongle.
\exitwrap

\begin{center}
\makebox[0pt]{
\begin{tabular}{|l|l|}
    \hline
    Name & Raspberry Pi model B type 3\\
    \hline
	Weight & $50\,\mathrm{g}$\\
    \hline
	Size & $90 \times 60 \times 20\,\mathrm{mm}$\\
    \hline
	Available at & \prog{https://www.electrokit.com/produkt/raspberry-pi-3-1gb-model-b/}\\
    \hline
\end{tabular}
}
\end{center}

%-------------------------------
\clearpage
\subsubsection{The wifi dongle}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/wifi.jpeg}
	\caption{\emph{Edimax EW-7612UAN V2\/} Wifi Module}
	\label{Fig:wifi}
\end{wrapfigure}
%% TODO link figure

\vskip\baselineskip
The \emph{Edimax EW-7612UAN V2\/} Wifi Module is a high-gain wireless USB adapter. It complies with wireless IEEE 802.11b/g/n standards. Plugged in the Raspberry Pi, it increased significantly the network speed.
\exitwrap

\begin{center}
\makebox[0pt]{
\begin{tabular}{|l|l|}
    \hline
    Name & Edimax EW-7612UAN\\
    \hline
	Weight & $30\,\mathrm{g}$\\
    \hline
	Size & $150 \times 15 \times 15\,\mathrm{mm}$\\
    \hline
	Available at & \prog{https://www.computersalg.se/i/1047967}\\
    \hline
\end{tabular}
}
\end{center}

%-------------------------------
\clearpage
\subsubsection{The speaker}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/speaker-real.jpg}
	\caption{Speaker}
	\label{Fig:speaker}
\end{wrapfigure}
%% TODO link figure

Instead of using a commercially available portable speaker, we decided to build our own speaker. This allows us to pick the speaker driver and its amplifier to make our speaker loud enough for our use. It takes its source input from the Raspberry DAC and it is powered by the battery. At first it was powered from the PDB but we noticed a lot of interference (due to the ESCs also connected to the PDB), so now it takes its power as close as possible from the battery itself. However it needs to be powered after the power module in order to not alter the battery measurement from the flight controller.
\exitwrap

It consists of the following parts:
\begin{itemize}
	\item the speaker driver
		%% TODO add amplifier references
	\item an amplifier. For the drones 1 to 5 we used: ... When building the drones 6 to 10 we couldn't find the same amplifier so we used ... instead. That second amplifier is much more powerful and we had to use a potentiometer to decrease the volume output.
	\item potentiometer (only on the drones 6 to 10)
	\item plywood $3\,\mathrm{mm}$
\end{itemize}

The enclosure in plywood is laser cut. The model of the parts (see Figure~\ref{Fig:speaker-model}) is available at ...
%% TODO add URL


% \begin{itemize}
% 	\item how loud can it be?
% 	\item the component list (says that we had to choose two different amplifiers for the drones 6 to 10)
% 	\item link the design: Figure~\ref{Fig:speaker-model}
% \end{itemize}
\exitwrap

\begin{figure}[p]
	\centering
	\includegraphics[width=1\textwidth]{./figures/hardware/speaker/speaker-parts.png}
	\caption{Custom speaker model}
	\label{Fig:speaker-model}
\end{figure}

\begin{center}
\makebox[0pt]{
\begin{tabular}{|l|l|}
    \hline
    Name & Custom Speaker\\
    \hline
	Weight & $271\,\mathrm{g}$\\
    \hline
	Size & 	TODO $150 \times 15 \times 15\,\mathrm{mm}$\\
    \hline
\end{tabular}
}
\end{center}

%-------------------------------
\clearpage
\subsubsection{The propeller protections}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/props-full-real.jpg}
	\caption{3D-printed propeller\\ protections fitted with screws\\ {~}}
	\label{Fig:protection}
\end{wrapfigure}
%% TODO link figure

We design the propeller protections (Figure~\ref{Fig:protection} and Figure~\ref{Fig:propCad}). Light and covering all the propellers blades were the main constraints. One protection comes in two parts: the bottom part (Figure~\ref{Fig:propCadbot} and the top part (Figure~\ref{Fig:propCadtop}). It is possible to put 5 MoCap markers on top of them (the size of the screws fit the markers). However there one major drawback in that design. It takes a lot of time to produce. Each part takes approximately 4 hours to print, which makes 8hours for only one protection. Each drone needs 4 protections and we have in total 10 drones. To produce all the necessary protections it took around 320 hours (a bit more than 13 days). In developpement we broke many sets of protections.

\exitwrap

\begin{figure}[hp]
	\centering
	\includegraphics[width=1\textwidth]{./figures/hardware/prop-full.png}
	\caption{CAD full propeller protection}
	\label{Fig:propCad}
\end{figure}
\begin{figure}[hp]
	\centering
	\includegraphics[width=1\textwidth]{./figures/hardware/prop-top.png}
	\caption{CAD propeller protection top}
	\label{Fig:propCadtop}
\end{figure}
\begin{figure}[hp]
	\centering
	\includegraphics[width=1\textwidth]{./figures/hardware/prop-bottom.png}
	\caption{CAD propeller protection bottom}
	\label{Fig:propCadbot}
\end{figure}
%% TODO link figures

%-------------------------------
\clearpage
\subsubsection{The DAC}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/dac.jpeg}
	\caption{Pimoroni pHAT DAC}
	\label{Fig:dac}
\end{wrapfigure}
%% TODO link figure

\vskip\baselineskip
The \emph{Pimoroni pHAT DAC} is a small high quality digital-to-analog converter for the Raspberry Pi. The default audio output of the Raspberry Pi cannot be used as it suffers from a lot of interference noises.
\exitwrap

%% TODO fill table with correct values
\begin{center}
\makebox[0pt]{
\begin{tabular}{|l|l|}
    \hline
    Name & Custom Speaker\\
    \hline
	Weight & $271\,\mathrm{g}$\\
    \hline
	Size & 	TODO $150 \times 15 \times 15\,\mathrm{mm}$\\
    \hline
	Available at & \prog{https://www.computersalg.se/i/1047967}\\
    \hline
\end{tabular}
}
\end{center}

%--------------
\clearpage
\subsubsection{The battery}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/battery.jpg}
	\caption{\emph{Tattu RC Battery} 8000 mAh 3S1P 15 Lipo Pack}
	\label{Fig:battery}
\end{wrapfigure}

We choose \emph{Tattu RC Battery 8000 mAh 3S1P 15\/} Lipo battery (see Figure~\ref{Fig:battery}).

A Lipo battery is characterized by its voltage (number of cells), its capacity, its discharge rate, its weight and its size.

\medskip
\textbf{voltage}.  A battery is constructed from rectangular cells which are connected together. Each cell holds a nominal voltage of $3.7\,\mathrm{V}$, so the voltage of such a battery is a multiple of $3.7$. A three-cell battery delivers $11.1\,\mathrm{V}$. The cell configuration of a battery is indicated by the notation $x\mathrm{S}y\mathrm{P}$ where $x$ is the number of cell sets in series and $y$ is the number of sets in parallel. In our case, our battery has 3 cells in series.

\medskip
\textbf{capacity}.  The capacity of a battery indicates how much energy it can store, measured in milli Ampere hours ($\mathrm{mAh}$). With $8000\,\mathrm{mAh}$, if we assume that our four motors consume their maximum continuous current $27.8\,\mathrm{A}$ then this battery can last only a little more than $4\,\mathrm{min}$.
%% fragment of sentence removed
% (which is very
In ordinary conditions, the motors should use a bit less that half of their maximum continuous current which gives between $10$ and $15\,\mathrm{min}$ of flight time.

\medskip
\textbf{discharge rate}.  The discharge rate of a battery defines how fast we can extract the energy from our battery. For our battery of discharge rate 15, it means that its maximum continuous current output is $120\,\mathrm{A}$. We verify that this number is bigger than the sum of the maximum continuous currents consumed by the drone components ($\sim 111.2\,\mathrm{A}$ in our case).

\medskip
\begin{center}
\makebox[0pt]{
\begin{tabular}{|l|l|}
    \hline
    Name & Tattu RC 8000 mAh 3S1P 15C Lipo Pack\\
    \hline
	Weight & $644\,\mathrm{g}$\\
    \hline
	Size & $169\times69\times27\,\mathrm{mm}$\\
    \hline
	Voltage & $11.1\,\mathrm{V}$ (3S1P)\\
    \hline
	Capacity & $8000\,\mathrm{mAh}$\\
    \hline
    discharge rate & 15C\\
    \hline
	Maximum Continuous Current & $240\,\mathrm{A}$\\
    \hline
	Available at & \prog{https://www.flyingtech.co.uk}\\
    \hline
\end{tabular}
}
\end{center}

%------------------
\clearpage
\subsubsection{The power distribution board (PDB)}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/pdb.png}
	\caption{Default PDB sold with the frame}
	\label{Fig:pdb}
\end{wrapfigure}
We use the default power distribution board given with the frame (see Figure~\ref{Fig:pdb}). It fits perfectly with the frame. However, it does not provide the connector to plug the flight controller \emph{Pixracer\/} which is indispensable since it communicates the state of the battery to the flight controller. That is why we need a power module.
\exitwrap

%-------------------
\clearpage
\subsubsection{The power module}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/power-module.jpg}
	\caption{mRo Hall Sens Power module ACSP7}
	\label{Fig:power-module}
\end{wrapfigure}
The power module (see Figure~\ref{Fig:power-module}) allows us to power the flight controller directly from the battery. That way the flight controller can measure the battery life. The drone cannot takeoff if the \emph{Pixracer\/} does not sense the battery or if the battery is too low.
\exitwrap

\begin{center}
\makebox[0pt]{
\begin{tabular}{|l|l|}
    \hline
    Name & mRo Hall Sens Power module ACSP7\\
    \hline
	Weight & $2.19\,\mathrm{g}$\\
    \hline
	Size & $17 \times 17,\mathrm{mm}$\\
    \hline
	Available at & \prog{https://store.mrobotics.io}\\
    \hline
\end{tabular}
}
\end{center}

\clearpage
\subsubsection{Hot Swap, Load Sharing Controller}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/Y-PWR-b.jpg}
	\caption{Y-PWR, Hot Swap, Load Sharing Controller}
	\label{Fig:ypwrb}
\end{wrapfigure}
The \emph{Y-PWR\/} (see Figure~\ref{Fig:ypwrb}) is used to perform a smooth transition between AC/DC wall adapter and battery to power the onboard computer, the flight controller, and the RC receiver. Thanks to this component, we can work on the onboard computer connected to the flight controller without powering the motors. For more detail on how it is used see Figure~\ref{Fig:wiring}
\exitwrap

\begin{center}
\makebox[0pt]{
\begin{tabular}{|l|l|}
    \hline
    Name &  Y-PWR, Hot Swap, Load Sharing Controller\\
    \hline
	Weight & $10\,\mathrm{g}$\\
    \hline
	Size & $66 \times 23,\mathrm{mm}$\\
    \hline
	Available at & \prog{http://www.mini-box.com/Y-PWR-Hot-Swap-Load-Sharing-Controller}\\
    \hline
\end{tabular}
}
\end{center}

%-------------------------------
\clearpage
\subsubsection{Poron XRD}
\begin{wrapfigure}{L}{0.5\textwidth}
	\centering
	\includegraphics[width=0.48\textwidth]{./figures/hardware/poron.jpg}
	\caption{Poron XRD}
	\label{Fig:poron}
\end{wrapfigure}
%% TODO link figure

Due to our design constraints we cannot put the pixracer on a traditional anti vibration mount preventing the vibrations from the frame to interfere with the sensors of the autopilot. So we decided to use a small sheet ($3\,\mathrm{mm}$) of poron XRD to isolate the flight controller (see Figure~\ref{Fig:poron}). Poron XRD with its open-cell technology is often used as a cushioning and impact protection solution (it is used in athletic shoes, etc.). Also, we put some poron between the battery and the drone frame. Indeed if the drone lands violently or crashes, this poron XRD sheet can protect the battery and our overall drone by absorbing the battery impact with the ground (or wall).
\exitwrap

\clearpage
\subsubsection{The extended frame}
\begin{itemize}
	%% TODO
	\item link the parts send to carbix to mill
\end{itemize}

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\textwidth]{./figures/hardware/designed-part1.png}
	\caption{Extended frame: body}
	\label{Fig:extended1}
\end{figure}
\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\textwidth]{./figures/hardware/designed-part2.png}
	\caption{Extented frame: feet}
	\label{Fig:extended2}
\end{figure}
\begin{figure}[htp]
	\centering
	\includegraphics[width=1\textwidth]{./figures/hardware/CAD-extended-frame.png}
	\caption{CAD Extended Frame}
	\label{Fig:extendedCad}
\end{figure}
%% TODO link figures

%% TODO missing figure cad
Now that all the parts have been selected, we create a CAD design to structure our assembly process. In Figure~\ref{cad}, from bottom to top we have the battery (in yellow), a level containing the Raspberry Pi (in black) with the Hot Swap, Load Sharing Controller (in salmon), the main body of the frame (in grey), the Pixracer (in red) and on top the speaker (in black). The parts to expand the frame have been cut and drilled by ourselves. %except the top one which is professionally cut and drilled by CarbiX \cite{carbix} due to its circular hole inside.

%% TODO missing figure real-drone
As we can see in Figure~\ref{real-drone}, we have not yet mounted the speaker. For the moment the top of the drone is used to place the markers used for the localization.
\egroup
%---- PARTS


%---- DESIGN
\subsection{The Design}
\subsubsection{The wiring}

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{./figures/hardware/wiring.png}
	\caption{Power distribution schematic}
	\label{Fig:wiring}
\end{figure}
The schematic representation in Figure~\ref{Fig:wiring} explains how the power is distributed in the drone, that is how all the electronic parts are connected. There are two possible sources of power but only the battery can power the motors and therefore make the drone takeoff. 

\clearpage
\subsection{The final design}
% \begin{figure}
% 	\centering
% 	\includegraphics[width=0.7\textwidth]{./figures/parts/drone-flying.jpg}
% 	\caption{Photo of the drone flying}
% 	\label{drone-fly}
% \end{figure}
The final design of our drones was found after several iterations. From bottom to the top we have:
\begin{itemize}
	\item \emph{Layer 1}: battery emplacement
	\item \emph{Layer 2}: electronic layer containing the Raspberry Pi with its DAC, the Y-PWR-B and the Pixracer.
	\item \emph{Layer 3}: main drone frame with its arms and its PDB.
	\item \emph{Layer 4}: the speaker.
\end{itemize}

Due to the \emph{Layer 1} and \emph{Layer 2}, the center of gravity of the drone is low increasing its stability.

The final characteristics of the drone are gathered in the following table.

\begin{center}
\begin{tabular}{|l|l|}
    \hline
	Weight without battery, without speaker, without propeller protections  & $453\,\mathrm{g}$ \\
    \hline
	Weight without battery, without speaker, with propeller protections & $589\,\mathrm{g}$ \\
    \hline
	Weight without battery with speaker and propeller protections & $860\,\mathrm{g}$ \\
    \hline
	Weight with battery, speaker and propeller protections & $1365\,\mathrm{g}$\\
    \hline
	Thrust to takeoff (per motor) & $341.5\,\mathrm{g}$ \\
    \hline
	Flight time (in theory) & $\sim 15\,\mathrm{min}$\\
    \hline
	Flight time (in practice) & $\sim 7\,\mathrm{min}$\\
    \hline
\end{tabular}
%\label{charac-drone}
\end{center}

This design is working: the drone is able to fly. The flight time in practice is an average of several flight times measured when the drone was performing a real choreography, including interactions with a dancer. 
