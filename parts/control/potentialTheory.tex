
\subsection{Design of the potentials}
% \label{section:potential-design}
In this section we explain the design of the potentials.
\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{./figures/control/schemat-goal-obstacle.png}
	\caption{Schematic representation of a drone with a goal and an obstacle}
	\label{goalobs}
\end{figure}

\subsubsection{Attractive potential}

\begin{figure}
	\centering
	\includegraphics[width=10cm]{./figures/control/attractive-potential.png}
	\caption{Shape of an attractive potential with its force}
	\label{attpotential}
\end{figure}

Let $M$ be the position of the considered drone, and $G$ its goal (see Figure~\ref{goalobs}). We choose as potential:
\begin{eqnarray}
	V &=& \gamma GM^2\\
	\vv{F} &=& -\grad V = -2\gamma\vv{GM}
\end{eqnarray}
where $\gamma$ is a constant that can be adjusted (see Figure~\ref{attpotential}). Here $\vv{GM}$ denotes the vector from $G$ to $M$, while $GM$ denotes its length, i.e., the distance from $G$ to $M$.
This potential corresponds to the usual attractive potentials in physics, like in a spring-mass system.

\subsubsection{Repulsive potential}

\begin{figure}
	\centering
	\includegraphics[width=10cm]{./figures/control/repulsive-potentials.png}
	\caption{Shape of the repulsive potentials with their forces}
	\label{repulpotential}
\end{figure}

Let $P$ be a punctual obstacle (see Figure~\ref{goalobs}). In physics, one usually works with potentials in $1/r$ (i.e. forces $\sim 1/r^2$) like the gravitation potential, however we decided to use potentials in $1/r^2$ (i.e. forces $\sim 1/r^3$). It means the potentials (resp. the forces) increase faster when the drone gets closer to the repulsive object.

At first, instead of using directly the potential usually used in the literature \cite{Khatib1980,Khatib1986} we chose as potential:
\begin{eqnarray}
\label{potentialV1}
	V &=& \frac\beta{\min(PM,\ell)^2+\varepsilon}\\
	\vv{F} &=& \begin{cases}
				\vv{0} \quad \textrm{if}\ PM > \ell \\
				2\beta\,\frac{\vv{PM}}{(PM^2+\varepsilon)^2} \quad \textrm{if}\ PM \leq \ell
			  \end{cases}
\end{eqnarray}
where $\beta$ and $\varepsilon$ are other constants. Adding $\varepsilon$ avoids getting infinite values in simulation (in practice, distance $PM$ should never get too close to zero!) Also, we limit the range of this force so that the obstacle has no influence on the movement of the drone if it is far enough: this is the purpose of $\min(PM,\ell)$ (see Figure~\ref{repulpotential}).
However, $V$ is not smooth in $PM = \ell$ which translates into a discontinuity in the force at that point. It is bad for the stability around that point.

That is why we decided to smoothen (\ref{potentialV1}) around $PM = \ell$ and we chose:
\begin{eqnarray}
\label{potentialV2}
	V &=& \beta\ \Bigg(\frac{1}{\min(PM,\ell)^2+\varepsilon} + \frac{\min(PM,\ell)^2}{(\ell^2+\varepsilon)^2}\Bigg)\\
	\vv{F} &=& \begin{cases}
				\vv{0} \quad \textrm{if}\ PM > \ell \\
				2\beta\, \bigg(\frac{1}{(PM^2+\varepsilon)^2} - \frac{1}{(\ell^2+\varepsilon)^2}\bigg) \vv{PM} \quad \textrm{if}\ PM \leq \ell
			  \end{cases}
\end{eqnarray}
which is slightly different from the classical choice \cite{Khatib1980}:
\begin{eqnarray}
\label{potentialV3}
	V &=& \beta\ \Bigg(\frac{1}{\min(PM,\ell)} - \frac{1}{\ell}\Bigg)^2\\
	\vv{F} &=& \begin{cases}
				\vv{0} \quad \textrm{if}\ PM > \ell \\
				2\beta\, \frac{\vv{PM}}{PM^3} \bigg(\frac{1}{PM} - \frac{1}{\ell}\bigg) \quad \textrm{if}\ PM \leq \ell
			  \end{cases}
\end{eqnarray}

Compared to (\ref{potentialV3}), the potential (\ref{potentialV2}) is slightly more repulsive when $PM$ is close to $\ell$ but then when $PM$ is getting smaller, (\ref{potentialV3}) becomes more repulsive. As (\ref{potentialV2}) does not offer any advantages compared to the classical potential, we decide to use the potential (\ref{potentialV3}).

The same potential (with possibly a different constant) can be used with non-punctual obstacles, such as the boundaries of the stage (both physical boundaries such as the ground and walls, and virtual boundaries such as the maximum flying altitude or a vertical plane in front of the stage to protect the audience).

In the case of the dancers, as only the positions of their head and hands are given by the MoCap system, we model them as a vertical segment between the head position and its projection on the ground, which may be too simplistic (see Figure~\ref{dancer-rep}). So if the drone is higher than them they act as a punctual obstacle, and if the drone is at their level or below, they act as a vertical line and the repulsion forces are horizontal. Also when the hands are far enough from this vertical segment, we create a repulsive half plane (vertical and containing the head and the hand) so that if the drones are in this half plane, the repulsion forces are horizontal.

\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{./figures/control/dancer.png}
	\caption{Representation of the dancer. The red dots are the positions tracked by the MoCap. The blue segment is our representation of the dancer body. The vertical dashed lines mark the distance from which the hands create repulsive half planes. Note: in this picture the right foot of the dancer is protected from the drones only as long as her right hand is extended.}
	\label{dancer-rep}
\end{figure}

We will give more details on the specific implementation of potentials for each object (drone, wall, ground, dancer, hand) in Section~\ref{section:potential-implementation}.

\subsubsection{Mixed potential}
\begin{figure}
	\centering
	\includegraphics[width=10cm]{./figures/control/mixed-potential.png}
	\caption{Shape of a mixed potential with its force}
	\label{mixpotential}
\end{figure}

If we instruct the drone to go to a point which is also an obstacle ($G = P$), the potential will be the sum of an attractive and a repulsive potential:

\begin{eqnarray}
\label{potentialV3-mixed}
	V &=& \gamma PM^2 + \beta\ \Bigg(\frac{1}{\min(PM,\ell)} - \frac{1}{\ell}\Bigg)^2\\
	\vv{F} &=& \begin{cases}
                -2\gamma\, \vv{PM} \quad \textrm{if}\ PM > \ell \\
				-2\gamma\, \vv{PM} + 2\beta\, \frac{\vv{PM}}{PM^3} \bigg(\frac{1}{PM} - \frac{1}{\ell}\bigg) \quad \textrm{if}\ PM \leq \ell
			  \end{cases}
\end{eqnarray}

This will result in a minimum at a certain distance, where the drone will settle (Figure~\ref{mixpotential}).

When several drones are given the same goal (which may itself be an obstacle or not), they will take positions on a sphere centered around that goal, the radius of which depends on the number of drones due to the repulsion between drones. For instance, if the dancer becomes a goal then the drones will place themselves above her head. Because our attractive potential is only punctual (on the head tracked by the MoCap), the drones cannot stay around her body as they will be attracted to go up toward the head.

% \subsubsection{Safety Measures}
% repulsive potential for the ground wall and the dancer. Explain the form of the repulsive field of the dancer. Which point from the dancer are tracked and the form of their potentials

% \subsubsection{A sample scene}

% The stage dimensions are $12\times6\times8$ meters. Drone $1$ is instructed to go to $(2, 2, 2)$ The dancer is at $(0, 2, 1.8)$ and two other drones are flying at $(1, 4, 2)$ and $(5, 2, 2.5)$.

% We represent the potential along the horizontal plane $z=2$ (Figure~\ref{pot-z2}) and the vertical plane $y=2$ (Figure~\ref{pot-y2}).

% \begin{figure}
% \centering
% \begin{subfigure}{1\textwidth}
%   \centering
%   \includegraphics[width=1\linewidth]{./figures/scene_potential_z=2.png}
%   \caption{Potential along the horizontal plane $z=2$}
%   \label{pot-z2}
% \end{subfigure}\\
% \begin{subfigure}{1\textwidth}
%   \centering
%   \includegraphics[width=1\linewidth]{./figures/scene_potential_y=2.png}
%   \caption{Potential along the vertical plane $y=2$}
%   \label{pot-y2}
% \end{subfigure}
% \caption{Representation of the potentials in our sample scene}
% \label{pot-scene}
% \end{figure}

% Observe that on the first plane, the dancer and drone $2$ are clearly visible, while drone $3$ being at a different altitude as a more limited effect. In the second plane, the dancer and drone $3$ are visible, while drone $2$ is too far and has no influence. The goal is in the darkest area.

% The forces resulting from the potential are represented as a vector field, along the same planes (Figure~\ref{for-z2}, Figure~\ref{for-y2}).

% \begin{figure}
% \centering
% \begin{subfigure}{1\textwidth}
%   \centering
%   \includegraphics[width=1\linewidth]{./figures/scene_forces_z=2.png}
%   \caption{Resulting forces along the horizontal plane $z=2$}
%   \label{for-z2}
% \end{subfigure}\\
% \begin{subfigure}{1\textwidth}
%   \centering
%   \includegraphics[width=1\linewidth]{./figures/scene_forces_y=2.png}
%   \caption{Resulting forces along the vertical plane $y=2$}
%   \label{for-y2}
% \end{subfigure}
% \caption{Representation of the potentials in our sample scene}
% \label{forces-scene}
% \end{figure}

% Depending on the initial position of drone $1$, it will pass in front of the dancer or behind her to reach its goal.

\subsection{Trajectory convergence}
In our model, we need to add a damping force to make the drone movements converge. Indeed, if we look at it with a physical point of view, without a damping, our system is conservative meaning its total energy is constant. When the drone gets closer to its goal, the potential energy of the system decreases while its kinetic energy increases. In that situation the drone will never stop at its goal.

In this part we do a theoretical study of our system to find out its optimal damping. We restrict our study to the unidimensional case.

Let us go back to our movement equation (\ref{speed-control}):
\begin{eqnarray}
	\label{speed-step}
	s_{n+1} &=& s_{n}\, (1-\lambda) + F_n \tau
\end{eqnarray}
If $\lambda = 1$ then the speed of the drone becomes proportional to the force $F$. It removes the integration of the speed and thus the inertia. The drones become more reactive but lose the smoothness of their movements. If $\lambda = 0$, as we said earlier we don't have any energy loss and the drone trajectories cannot converge. The optimal $\lambda$ is thus strictly between $0$ and $1$.

In this simple analysis we assume that the drones react immediately (i.e. with no latency) and faithfully to the speed command. The position of the drone then satisfies the following relation:
\begin{eqnarray}
    \label{pos-step}
	x_{n+1} &=& x_{n} + s_n \tau
\end{eqnarray}

\subsubsection{Case 1: attractive force}
\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{./figures/control/convergence.png}
	\caption{Convergence cases for only one attractive point at $x=0$. ``CV'' means that the trajectory converges and ``DIV'' means that the trajectory diverges}
	\label{fig:convergence}
\end{figure}
We study the case when there is only one attractive point at $x=0$. The system is then governed by equations \ref{pos-step} and \ref{speed-step} where:
\begin{eqnarray}
	F_n = -2\gamma\, x_n
\end{eqnarray}
Setting $U_n =\begin{bmatrix}x_n \\ s_n\end{bmatrix}$ we get $U_{n+1} = A\, U_n$ with
\begin{eqnarray}
	A=\begin{bmatrix} 1 & \tau \\ -2\gamma\tau & 1-\lambda\end{bmatrix}
\end{eqnarray}
The characteristic polynomial is $X^2 - (2-\lambda)X + (1-\lambda+2\gamma\tau^2)$ and its discriminant is $\Delta = \lambda^2 - 8\gamma\tau^2$. We notice that $\Delta < \lambda^2$.

When $\Delta>0$ the system has two real eigenvalues in the interval $(0,1)$. So it converges slowly without any overshoot.

When $\Delta<0$ the system has two conjugate complex eigenvalues with modulus $\sqrt{1-\lambda+2\gamma\tau^2}$. Therefore if $\lambda \leq 2\gamma\tau^2$ the system diverges. And if $\lambda > 2\gamma\tau^2$ it converges with oscillations.

Finally, when $\Delta = 0$ the system has one double eigenvalue $1-\lambda/2$ and converges quickly. This is the fastest convergence rate for fixed $\lambda$.

If $2\gamma\tau^2 \geq 1$ then no choice of $\lambda$ can ensure convergence (see Figure~\ref{fig:convergence}).

If $1/4 \leq 2\gamma\tau^2 < 1$, then $\lambda > 2\gamma\tau^2$ ensures the best convergence but we cannot avoid oscillations (see Figure~\ref{fig:convergence}).

If $0 < 2\gamma\tau^2 < 1/4$, then we can take $\lambda = \sqrt{8\gamma\tau^2}$ to get a fast convergence without oscillations (see Figure~\ref{fig:convergence}).
 


\subsubsection{Case 2: mixed force}
Now, we assume that there is an obstacle between the drone and the attractive point. Assume all objects are on the $x$ axis, with the obstacle at position $0$, the goal at position $g<0$ and the drone at position $x>0$.

%%TODO draw a picture

For $0 < x \leq \ell$, the force is:
\begin{eqnarray}
	F = -2\gamma\, (x-g) + \frac{2\beta}{x^2}\, \bigg(\frac{1}{x} - \frac{1}{\ell}\bigg)
\end{eqnarray}
It is a decreasing function of $x$, with $F\rightarrow +\infty$ when $x\rightarrow 0$ and $F<0$ when $x=\ell$. Therefore there is a unique equilibrium point $x_0$ in $(0,\ell)$.
We do a first order estimation of $F$ around $x_0$:
\begin{eqnarray}
	F = -2\bar\gamma(x-x_0) + O\big((x-x_0)^2\big)
\end{eqnarray}
where $\bar\gamma = \gamma + \beta\, \bigg(\frac{3}{x_0^4} - \frac{2}{x_0^3\ell}\bigg)$. Then we see that the behaviour will be the same as in the attractive case with $\gamma$ replaced with $\bar\gamma$. Since $x_0<\ell$ we have $\bar\gamma > \gamma + \frac{\beta}{\ell^4}$.

If $\lambda$ has been tuned to have $\Delta = 0$ in case 1, then we get oscillation (or even divergence) in case 2. Therefore it may be necessary to choose a higher $\lambda$.

\subsection{Prediction Step}
In our system we have some latency to take into account. Indeed, between the time the position of the drone is measured by the MoCap and the time the drone executes our control command there is a delay.

For the moment, because we did not create a model of our drone, we use a simple linear extrapolation for a fixed delay by using the previous position of the drone. More complex and accurate predictions can be implemented. One such method is described in \cite{Lupashin2014-platform}, where they are also using the list of commands sent to the flight controller. 

A similar prediction step is also needed for other moving objects (the dancer and other drones). In this case we have no other information than the past positions.
