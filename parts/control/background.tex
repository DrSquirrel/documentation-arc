
Motion planning is an essential problem in robotics and especially in robot automation. From an initial configuration (position, orientation) to a final configuration, its aims is to find a collision-free motion with its set of inputs (forces, torques, etc.) in a specified environment. In other words, motion planning is the process of selecting, among all possible motions and all their possible input sets, a motion with a corresponding set of inputs that ensures that all constraints are satisfied. Therefore, it depends on the model of the robot and its environment. The process to follow a planned motion is called \textit{control\/} and it is not the focus of this section.

Several ways to classify motion planning strategies are proposed in the literature. One way is to distinguish global methods from local ones. Global strategies ensure that if there exist collision-free trajectories between the current robot configuration and its goal, the robot will try to follow one such trajectory. Usually, such strategies plan in advance the trajectory, and try to make it optimal for some criterion (shortest distance, shortest time, least energy consumption, etc.). In contrast, local strategies aim to avoid unexpected obstacles (unmodelled or moving) by reacting to the robot environment. However, we decide to follow another classification \cite{Kumar2007-motion-planning} that distinguishes implicit motion plans from explicit ones.

\subsection{Implicit methods}
Implicit motion strategies do not explicitly compute the trajectories before the motion starts. The motion is never pre-computed. Instead they define how the robot interacts with its surroundings by specifying how it responds to its sensory information.

\paragraph{Potential field.} One such method is the potential field method \cite{Khatib1980,Khatib1986}. In this method an artificial potential is associated to all the obstacles and to the goal, similar to the potential associated to conservative physical forces such as electrostatic forces. The robot has to follow the negative gradient of the potential until it reaches a local minimum, for instance by taking the negative gradient as input for a velocity controller.

The potential function can be freely chosen and does not need to model any existing physical force. However, it has to be smooth enough, minimal at the goal and maximal at the obstacles. Usually, this potential is chosen with limited range so that the obstacle does not interfere with the motion of the robot when the robot is far from it. For instance, the following formula is often used for the repulsive potential and the associated force:
\begin{eqnarray}
\label{potential-Khatib}
	V &=& \beta\ \Bigg(\frac{1}{\min(q,\ell)} - \frac{1}{\ell}\Bigg)^2\\
	\vv{F} &=& \begin{cases}
				\vv{0} \quad \textrm{if}\ q > \ell \\
				-2\beta\, \frac{\vv{q}}{q^3} \bigg(\frac{1}{q} - \frac{1}{\ell}\bigg) \quad \textrm{if}\ q < \ell
			  \end{cases}
\end{eqnarray}
where $\vv{q}$ is the vector from the obstacle to the robot, $\ell$ is the (constant) range of the force and $\beta$ a coefficient.

This method has several advantages: it is computationally light, no prior processing is needed, it adapts well to unexpected environment changes.
It has also several drawbacks \cite{Koren1991-potential}: the robot may not reach its goal when it is trapped in another local minimum, it may not find a passage between obstacles that are near each other, and it may oscillate when it is close to obstacles. The resulting trajectory can be very different from trajectories obtained by other algorithms and far from optimal for usual measures. It is difficult to integrate the kinematic and dynamic constraints of the robot.
%nonholonomic contraint is difficult

In order to avoid being trapped by local minima one can use harmonic potentials (that have only one minimum) or a navigation function \cite{Rimon-Koditschek}.
% \begin{itemize}
% 	\item velocity controller (2 cases: either we integrate with the previous speed with a damping < 1 or directly speed)
% \end{itemize}

\subsection{Explicit methods}
Explicit motion planning strategies explicitly compute the complete trajectory by producing a set of sub-goals. They plan the trajectory from start to goal before the motion occurs. We can further divide explicit strategies between the discrete ones and the continuous ones.
\subsubsection{Discrete methods}
Such methods discretize the configuration space and represent it with a graph. Motion planning is therefore reduced to a graph search problem. As the potential method, discrete explicit strategies do not accommodate for kinematic and dynamic constraints of the robot. However a two-step strategy could be used: first find a suitable motion plan with the method and then restrict it to make sure it meets all the constraints of the robot. Among these methods we find two types of algorithms: road map and cell decomposition.

\paragraph{Road map.} Road map algorithms consist in constructing a set of curves that connect the space. Then finding a path between two points comes down to connecting these two points to the connectivity graph. Methods such as visibility graph, Voronoi graph, etc. can then be used.

\paragraph{Cell decomposition.} Cell decomposition algorithms subdivide the configuration space into non-overlapping cells and then construct a connectivity graph based on the neighbour relations of these cells. To find a path, we simply need to connect the cells from the start to the goal, using a shortest path algorithm.

\subsubsection{Continuous methods}
Continuous explicit methods are similar to open loop control laws. There are two types of continuous methods: specific methods for nonholonomic systems (also called underactuated systems), and optimal methods.

\paragraph{Underactuated systems.} The methods in this family, based on controllability tests, provide ways to generate motion plans for nonholonomic systems \cite{Li-Canny}. Also known as \textit{steering methods}, they come from nonlinear control \cite{Brockett1982, Sussmann1983}. Some of these methods can be used for dynamic motion planning \cite{Bloch1996, Ostrowski1995}.

\paragraph{Optimal methods.} Finding an optimal trajectory for a constrained robot is find a solution to a minimization problem (optimal control) \cite{Laumond1994}. For solving optimal control problems, we often need to use numerical methods, such as the \textit{method of neighbouring extremals\/} \cite{Bryson-Ho}, conjugate gradients algorithms \cite{Jacobson1968, Lasdon-Mitter-Warren, Ma-Levine, Mayne-Polak}, discretization \cite{Teo-Goh-Wong, Pytlak-Vinter}, or finite-elements techniques \cite{Zefran1996}.

%-----------------------------------------------------
\subsection{Discussion}
In our project, the drones live in a dynamic environment. Apart from the fixed restrictions of our flying area (ground, artificial walls and ceiling) the drones have to avoid each other and the moving dancer. Also, unlike in typical motion planning problems, our drones do not require optimal motions. Instead, we would like them to have natural and smooth motions suitable to express emotions and intents. This is why we have decided to use an implicit motion planning method, namely potential fields. 

