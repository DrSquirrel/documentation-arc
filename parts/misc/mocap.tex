In this section we present our flying area. First, we describe its current location, its position tracking system, its network/software setup and its safety measures. Second, we analyse and discuss its characterisitics.

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{./figures/misc/mocap/stage.JPG}
\caption{MoCap setup in R1}
\label{Fig:mocapR1}
\end{figure}

%---------------------------------------------------------
\subsection{The location}
When the project started in September 2016, the RPL lab didn't have a flying area for drones. And because the drone choir is meant to be performed indoors at different locations, we decided to setup an independent flying area for this project. This area had to be large enough to allow five drones and one dancer to wander freely inside.

Thanks to the artistic value of this project, we settled down in KTH R1. R1 was Sweden's first experimental nuclear reactor. It was in operation until 1970. After its dismantling around 1982 and its radioactive decontamination period, it became in 2007 part of KTH as a cultural experimental center. Focused on the interconnection between science and art, a number of different installations, performances and projects have been carried out there.

However the location of this project is meant to change depending on where it is performed. Indeed, in March 2019, the MoCap system was moved to Rijeka and set up inside Croatian National Theatre Ivan pl. Zajc.
%---------------------------------------------------------
\subsection{Hardware}
\begin{figure}
	\centering
	\includegraphics[width=0.2\textwidth]{./figures/misc/mocap/flex13.png} \quad
	\includegraphics[width=0.7\textwidth]{./figures/misc/mocap/mocap-wiring.png}
	\caption{OptiTrack Flex 13 motion capture camera (on the left) and its wiring to USB camera system (on the right)}
	\label{Fig:optitrack}
\end{figure}

To track the position of the quadrotors we use an OptiTrack Motion Capture System (MoCap). Currently it consists of 12 OptiTrack Flex 13 Cameras (see Figure~\ref{Fig:optitrack}). Each camera has $56\degree$ of horizontal field of view (FOV) and $46\degree$ of vertical FOV. When paired with the biggest available marker, the maximum tracking distance can reach up to $12\,\mathrm{m}$. The advertised tracking volume on OptiTrack's website is $7 \times 7 \times 2 \,\mathrm{m}$. In the analysis section, we will discuss our capture volume compared with that one.

The wiring schematic is represented in Figure~\ref{Fig:optitrack}. The cameras are connected to a hub which is connected to a Windows laptop. Both connections are in USB. If we use several hubs, then we need to connect the hubs together via a RCA to RCA cable to synchronize them. Between a camera and the hub we can use only one USB cable which means that the maximum distance between those two is $5\,\mathrm{m}$. In R1 we put the cameras on each wall (see Figure~\ref{Fig:mocapR1}). However the distance between these two walls is longer than $11\,\mathrm{m}$ which is the longest RCA-RCA synchronization cable sold by OptiTrack. Because finding a longer RCA-RCA cable with a low impedance (video usage) was difficult, we decided to buy another RCA-RCA cable and simply link the two synchronization cables with an RCA-RCA connector. It worked.

%---------------------------------------------------------
\subsection{Software}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{./figures/misc/mocap/setup.png}
\caption{Schematic of our MoCap setup}
\label{Fig:mocap-setup}
\end{figure}

The MoCap system comes with its software \prog{Motive} which is exclusively running in Windows. \prog{Motive} is used to calibrate the cameras, control and supervise our tracking volume and stream the tracking data to a Linux laptop (Debian Jessie) via an Ethernet cable (see Figure~\ref{Fig:mocap-setup}). \prog{Motive} uses the NatNet streaming protocol to stream the tracking data.

We use the \prog{ROS} package \prog{mocap\_optitrack} to parse the incoming streaming data from \prog{Motive} into tf transforms and poses usable in the \prog{ROS} environment.

We setup a local network which is not connected to the Internet inside R1, so that the Linux laptop can communicate with the drones via wifi. Because each drone has an onboard computer also running \prog{ROS}, the communication between the Linux laptop and the drones consists of \prog{ROS} internal messages. And the onboard computer will translate the flight instructions to the flight controller via the MAVLink protocol thanks to the package \prog{mavros}.

% \begin{itemize}
% 	\item orientations ?
% 	\item frame id issues ?
% 	\item speak of the mis alignment?
% \end{itemize}
%---------------------------------------------------------
\subsection{Safety measures during developpement}

In the flying area, safety measures are important not only to protect the surrounding humans from hazardous drone behaviours but also to prevent the drones from destructing themselves when they crash.

On the ground we put six foam mattresses ($210 \times 182 \times 35\,\mathrm{mm}$) to cover an area of $22\,\mathrm{m}^2$. We restrict the flying area to the surface covered by those mats so that it now measures $4.20 \times 5.46\,\mathrm{m}$. To protect the mats we had planned to put a plastic cover over them. However it turned out that the plastic was interfering with the tracking system and had to be removed. Instead we use simple black bed sheets. They provide protection for the mats against dust, add more resistivity for the drone landings/crashes, and allow a better control of the tracking area regarding unwanted reflections.

It was not possible to put nets around the flying area. So we decided to attach each drone to a weight via a fishing wire. The wire is light enough to not disturb the drone flight and rigid enough to prevent it from going beyond the flying area borders.

\subsection{Safety measures: Software}
The drones rely heavily on the MoCap. However in some cases the MoCap can give some inconsistent and sometimes even wrong data. So we had to implement some security checks when receiving its data in our ROS nodes.

DancerCallback:
\begin{itemize}
	\item dancer position jumping when his speed (mocap) is $> speed\_dancer\_reliable$
	\item Dancer position is too close to the drone when dist $< dist\_me$
\end{itemize}

HandCallback:
\begin{itemize}
	\item Hand position is too close to the dancer $< dist\_me$
	\item Hand position is too far from the dancer $> dist\_dancer$
\end{itemize}

MocapCallback:
\begin{itemize}
	\item compare with the current local position given by the flight controller. If $>dist\_diverge$ for more than \prog{mocap\_diverge\_count} then they diverge
	\item check if the mocap freezes. If $< mocap\_freeze\_dist$ for more than \prog{mocap\_freeze\_count} then the MoCap is considered freezing.
	\item check the speed given by the mocap for the other drones, if their speed $> speed\_reliable$ then mocap jump.
\end{itemize}

%---------------------------------------------------------
\subsection{Discussion}
In our current setup the disposition of the cameras is not optimal. The volume covered by a MoCap depends heavily on how the cameras are positioned, which depends on where we setup our flying area. In R1, to maximize the stage area, we needed to put the cameras on the walls. However, by doing so, the mattress area which is half of the maximum advertised covered area is at the border of our tracked volume. Also, by having the cameras that far from the center of the stage ($11\,\mathrm{m}$), it makes it hard to track the drone for two reasons: first, it is difficult to place the markers on the drone so that the MoCap software does not consider two different markers as one due to the camera angle views. Second, as soon as a dancer steps into the flying area, he or she interferes with the tracking volume: in some positions the dancer can prevent the MoCap from tracking the drone position.
We are conscious that our security measures are really rudimentary (no net). In fact, the main part of our safety measures relies on our control based on potential fields. Attaching the drone to a weight with a wire is only used for testing experimental features and making sure that our potentials can keep the drone and the dancer safe.
