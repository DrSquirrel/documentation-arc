
In order to design our potentials and tune our control parameters, having good plotting and visualization tools is essential. \prog{ROS}
provides packages like \prog{Rviz} and \prog{rqt\_plot} but they are not suited for potential field representation and are focused on live visualization instead of a posteriori analysis. That is why we decided to implement a node \prog{Recorder} which records all the useful data during the flight time (either in simulation or in reality) into files that can be used later for plotting. We implemented two visualization tools: one that plots the trajectories as a function of time and another that represents the potential fields as heat plots and vector fields. The plots are done by \prog{gnuplot} which is a freely distributed portable command-line driven graphing utility.

In fact, each time we run the simulation or make the drones fly, we record in a folder uniquely named by the date and time of the recording the following data:
\begin{itemize}
	\item for each drone (numbered $i$):
		\begin{itemize}
			\item its positions from the MoCap (or MoCap simulation) in the file \prog{uav$i$}
			\item its goals in the file \prog{goal$i$}
			\item its acceleration (as computed from the potential field) in the file \prog{accl$i$}
			\item its twist (as sent to the PX4, with damping applied) in the file \prog{twist$i$}
			\item its predicted position in the file \prog{predict$i$}
		\end{itemize}
	\item the position of the dancer (\prog{dancer\_head})
	\item the position of the dancer hands (\prog{right\_hand}, \prog{left\_hand})
	\item the control parameters in \prog{params}
	\item the choreography played (list of timestamped instructions the drones were supposed to follow) in the file \prog{event}
\end{itemize}
Then, a Perl script goes through those files and computes a program that is given as input to \prog{gnuplot}.

\subsection{Automatic trajectory plotting}
This tool plots two types of figures: a global view of the trajectories (e.g. Figure~\ref{figure:I1-global-sim}) and a view restricted to the \prog{x}, \prog{y} and \prog{z} axis (e.g. Figure~\ref{figure:I1-coord-sim}). The time interval for plotting is by default restricted to the interval between the first and the last events in the \prog{choreographer} script. However, it is possible to manually specify a time interval for more accurate plotting.

\paragraph{The global view.} In this representation, the 3D trajectories of all the elements (drones, dancer, etc.) present during the recording are plotted. Also, every $0.5\,\mathrm{s}$ we plot the computed speed vector on the drone trajectories. It allows to correlate the real trajectories with our control input and thus detect and analyse any anomalies or suspicious behaviours.

\paragraph{The coordinate view.} For each drone, we plot according to the \prog{x}, \prog{y} and \prog{z} axis, its position given by the MoCap, its predicted position, its goals. Also, we add the positions of the walls/ceiling and of the dancer, and we mark with a vertical segment the timestamp of each new instruction in the \prog{choreographer} script. It allows us to visualize the overshoot or/and oscillation with respect to each axis in the drone trajectories. Also it helps us evaluate and possibly re-design our position prediction procedure.

\subsection{Automatic potential visualization}
This tool aims to represent the potential fields.
It plots two types of figures: heat maps, representing the potential itself (e.g. Figure~\ref{figure:I3-pot1}), and vector fields, representing the resulting forces (e.g. Figure~\ref{figure:I2-pot1}).
It does this for each drone, as each drone has its own potential field, and at each new event in the choreography.
It is also possible to specify additional times for which a plot should be made.

Since the potential is defined in three-dimensional space, it is not possible to represent it directly (a 3D representation of the vector field is possible, but not very legible).
Therefore we have two make cuts.
In order to capture relevant features of the potential, we chose two variable cut planes: first, the horizontal plane at the altitude of the considered drone; second, the vertical plane containing both the drone and its goal, or a frontal plane if there is no goal or if the drone is too close to the goal. This second figure uses a horizontal coordinate system centered on the drone, and has variable width.

Each figure indicates the cut plane of the other figure, as well as
the projected position of other drones and of the dancer.

A difficulty arose in the representation of the potential. The repulsive potentials, as designed in Section~\ref{section:potential-design}, tend to infinity when approaching the repulsive point. But in our implementation, we decided to truncate the forces so that the acceleration of the drone remains reasonable, and this amounts to modify the potential field. However, to control the drone we did not need to actually compute this modified potential, as we instead compute directly its gradient. So we had to compute the modified potential specifically for visualization. It is not possible to determine algebraically at which position the gradient reaches its maximum, so for this we have to numerically solve an equation. This is done in the Perl script, then the value is plugged into the equation that is passed to \prog{gnuplot}.
