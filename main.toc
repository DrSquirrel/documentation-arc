\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Related Works}{7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}General view of the flying platform}{10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Overview}{11}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}The Hardware}{12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}The design of the drones}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Constraints}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.2}The parts}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The frame}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The motors}{14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Brushless vs DC Motors}{14}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Characteristics}{14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The flight controller}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The propellers}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The ESC}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The onboard computer}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The wifi dongle}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The speaker}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The propeller protections}{24}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The DAC}{26}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The battery}{27}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The power distribution board (PDB)}{28}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The power module}{29}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Hot Swap, Load Sharing Controller}{30}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Poron XRD}{31}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The extended frame}{32}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.3}The Design}{35}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The wiring}{35}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.4}The final design}{36}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}In Practice and Reflexions}{36}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}The Estimation and Control Library (PX4)}{37}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Pose estimation}{37}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Hardware.}{37}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Parametrization.}{38}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Analysis of the estimators}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Q Attitude estimator and Local Position Estimator}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Q estimator}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Local Position Estimator (LPE)}{41}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}EKF2 estimator}{44}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The EKF2 module of the PX4}{44}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline The algorithm and its implementation}{46}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Experiments and Discussion}{47}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Motivation}{47}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}The setup}{48}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}The experiments}{48}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.4}Discussion}{49}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Reflexions}{49}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Motion Control}{50}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Background}{50}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Implicit methods}{50}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Potential field.}{50}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Explicit methods}{51}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Discrete methods}{51}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Road map.}{51}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Cell decomposition.}{51}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Continuous methods}{52}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Underactuated systems.}{52}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Optimal methods.}{52}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.3}Discussion}{52}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Trajectory control: Using potentials}{52}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Design of the potentials}{52}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Attractive potential}{52}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Repulsive potential}{54}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Mixed potential}{55}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}Trajectory convergence}{57}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Case 1: attractive force}{57}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Case 2: mixed force}{59}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}Prediction Step}{59}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Securities Considerations}{59}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Reflexions}{60}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}The Script Language}{61}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Language description}{61}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.1}Instructions}{61}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.2}Parallelism}{64}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.3}Events}{64}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Dancer Events.}{64}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline System Events.}{65}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.4}Procedures}{66}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1.5}Control flow}{67}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Scene Analysis}{68}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.1}Scene 1}{68}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2.2}Scene 2}{70}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Implementation Aspects}{76}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}In Practice and Reflexions}{77}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}ROS Implementation}{78}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Overview}{78}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}ROS Launches}{79}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.3}ROS Nodes}{81}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.1}Choreographer node}{81}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{81}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{81}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{82}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.2}Stage security node}{82}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{82}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{82}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{82}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.3}Trajectory recording node}{83}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{83}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{83}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{84}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.4}MoCap node}{84}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{84}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{84}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{85}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.5}Event trigger node}{85}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{85}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{85}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{85}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.6}Music node}{86}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{86}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{86}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{86}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.7}MoCap simulation node}{86}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{86}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{87}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{87}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.8}Dancer simulation node}{87}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{87}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{87}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{87}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.9}MoCap calibration node}{87}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{87}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{88}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{88}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.10}MoCap duplication node}{88}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{88}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{88}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{88}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.11}Apply motions node}{89}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{89}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{89}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{89}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.12}General controller node}{90}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{90}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{90}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{91}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.13}Flight mode node}{93}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{93}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{93}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{93}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.14}Mavros node}{93}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{93}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{94}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{95}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.15}Play node}{96}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{96}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{96}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{96}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3.16}Record node}{96}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Description}{96}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Topics}{97}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\nonumberline Parameters}{97}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.4}ROS Topics}{97}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.5}ROS Parameters}{104}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Miscellaneous}{105}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Auxiliary scripts}{105}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.1}Setting up the drones}{105}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.2}Plotting}{106}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.3}ROS utilities}{106}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1.4}Manual intervention}{107}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Visualisation}{107}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.1}Automatic trajectory plotting}{108}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline The global view.}{108}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline The coordinate view.}{108}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2.2}Automatic potential visualization}{108}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Music}{109}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.4}Networks}{109}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.5}MoCap}{109}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5.1}The location}{109}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5.2}Hardware}{110}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5.3}Software}{111}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5.4}Safety measures during developpement}{111}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5.5}Safety measures: Software}{112}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5.6}Discussion}{113}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Tutorials}{114}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}MoCap Setup}{114}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2}Network Setup}{118}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3}Managing Music and playlists}{120}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.4}Start Linux Laptop ROS nodes}{122}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.5}Start Drone ROS nodes}{124}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.6}Before real flight}{125}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.7}Scene1}{126}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.8}3D Printing}{128}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.9}Github}{129}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.10}Batteries}{130}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.11}I3wm basics}{131}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.12}How to build the drones?}{132}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.13}Annoying Beep?}{133}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {9}Conclusions}{134}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Bibliography}{135}
