#--------------------
# Functions
#--------------------

## init: a drone gets ready to take off
## duration: 0 s
def init
when takeoffArea fly

## fly: when in take off area, a drone plays music,
## takes off and goes to its base position
## duration: 8 s
def fly
when isReadyToGo init
music play 3
mode 1
upto 2.5
sleep 1
offboard
sleep 2
uav 6 point -1.8 1.2 2.5
uav 7 point -1.6 2.8 2.5
uav 8 point 0 1.2 2.5
uav 9 point 1.5 1.2 2.5
uav 10 point 1.5 2.8 2.5
sleep 3
mode 6
look public
predrop
sleep 2

## randomfall: a drone falls after a random wait
## duration: variable, average 10 s
def randomfall
when isReadyToGo restart
repeat 0
  random 20 fall
  sleep $interval
end

## fall: a drone falls, then raises somewhat after a random wait
## duration: variable, minimum 8 s, average 13.2 s
def fall
when cantDrop canceldrop
music play 3
sleep 4
music play 2
upto 1
drop
sleep 4
repeat 0
  random 50 level1
  random 49 level2
  random 48 level3
  random 47 level4
  random 46 level5
  sleep $interval
end

## level1: a drone raises to 1.3 m, then raises again after a random wait
## duration: variable, minimum 1 s, average 6.2 s
def level1
music stop
upto 1.3
sleep 1
music play 3
repeat 0
  random 40 level2
  random 39 level3
  random 38 level4
  random 37 level5
  sleep $interval
end

## level2: a drone raises to 1.6 m, then raises again after a random wait
## duration: variable, minimum 1 s, average 6.2 s
def level2
music stop
upto 1.6
sleep 1
music play 3
repeat 0
  random 30 level3
  random 29 level4
  random 28 level5
  sleep $interval
end

## level3: a drone raises to 1.9 m, then raises again after a random wait
## duration: variable, minimum 1 s, average 6.1 s
def level3
music stop
upto 1.9
sleep 1
music play 3
repeat 0
  random 20 level4
  random 19 level5
  sleep $interval
end

## level4: a drone raises to 2.2 m, then raises again after a random wait
## duration: variable, minimum 1 s, average 6 s
def level4
music stop
upto 2.2
sleep 1
music play 3
repeat 0
  random 10 level5
  sleep $interval
end

## level5: a drone raises to 2.5 m, then resumes falling randomly
## duration: 2 s
def level5
upto 2.5
sleep 1
music stop
sleep 1
randomfall

## canceldrop: if the drone cannot drop, abort
## and recenter before resuming dronefall
## duration: 2 s
def canceldrop
music stop
mode 1
point 0 2 2.5
sleep 0.5
mode 6
sleep 1
upto 2.5
mode 1
sleep 1
mode 6
randomfall

## finishing: a drone returns to top level, waits, then lands
## duration: 5 s
def finishing
music stop
mode 1
upto 2.5
sleep 5
land


# restarting
def restart
upto 2.5
offboard
sleep 4
mode 6
randomfall
